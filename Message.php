<?php
  
  include 'Scripts/Include.php';
  SetSettings();
  CheckAuthorisation('Message.php');
  
  //////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Message');
      //////////////////////////////////////////////////////////////////////////
    ?>
      
      <link href="Stylesheets/dx.common16.1.8.css" rel="stylesheet" type="text/css"/>
      <link href="Stylesheets/dx.light-compact.css" rel="stylesheet" type="text/css"/>
      <link href="Stylesheets/chosen.css" rel="stylesheet" type="text/css"/> 
      <meta http-equiv="content-type" content="text/html; charset=utf-8">
      <script src="Scripts/DevExpress/js/jquery-2.1.3.min.js" type="text/javascript"></script>
      <script src="Scripts/chosen.jquery.js" type="text/javascript"></script>
      <script src="Scripts/FileScripts/Message.js"></script>        
        <style>

            
            #backbuttonsection{
                width: 650px;
                margin: auto;
                text-align: right;

            }
              .Unsuccessful
            {
                padding: 1em;
                background-color: #ff4d4d;
                color: black;
                opacity: 1;
                transition: opacity 0.6s;
                z-index: 5;
                position: absolute;
                margin: auto;
                text-align: center;
                padding: 0.5em;
                width: 550px;
                top: 36%;
                left: calc(50% - 225px);
            }
            
             .alert 
            {
                padding: 1em;
                background-color: #66ff66;
                color: black;
                opacity: 1;
                transition: opacity 0.6s;
                z-index: 5;
                position: absolute;
                margin: auto;
                text-align: center;
                padding: 1em;
                width: 550px;
                top: 40%;
                left: calc(50% - 225px);
            }
            
            #gridContainer{
                 width: 80%;
                  margin: auto;
                 
            }
            
            .dx-datagrid-rowsview.dx-datagrid-nowrap.dx-scrollable.dx-scrollable-customizable-scrollbars.dx-scrollable-both.dx-scrollable-simulated.dx-visibility-change-handler{
                 overflow-y: scroll !important;
            }
               
            .Buttonsection{
                width: 100%;
                text-align: right;
                padding-right: 10%;
                float: right;
                padding-top: 1.5em;
               }
               
            #addSection{
                    width: 80%;
                    text-align: right; 
                    margin: auto !important;;
               }
               
            #popAlert{
                   display: none;
               }
               
            #alert{
                   min-height: 20px;
                   line-height: 20px;
                   text-align: center;
                   border: #FF0000 2px solid;
                   color: #FF0000;
               }
            
            #duplicate_name{
                   color:red;
                   display: none;
                   padding-left: 0.5em;
               }      
           
            .tableAdd
            {
                border: none;    
                padding: 0.2em;
                width: 100%;
            }
           
            #box
            {   
            width: 97%;
            padding:1%;
            height: 175px; 
            border: solid;
            border-color: darkblue;
            border-width: thin;
            overflow-y: scroll;
            margin: auto;
            border-collapse:collapse;
            }  
            
            .Hide
            {
                display:none;
            }
            
            .Long
            {
                width: calc(100%);
                border-radius: 4px;
                margin-right: 1em;
                box-shadow: 0 1px 3px transparent;
                border-width: 1px;
                border-style: solid;
                background-color: #fff;
                border-color: #ddd;
                color: #333;
                border-radius: 4px;
                height: 30px;
            }
            
            .selectDrop{
                width: 250px !important;
                text-align: left;
            }
   
      </style> 
        
        
        
  </HEAD>
  <BODY>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>
    <DIV class="contentcontainer">
      <?php 
        // PHP SCRIPT ////////////////////////////////////////////////////////// 
        BuildMenu('Main', 'Message.php');                               
        ////////////////////////////////////////////////////////////////////////
      ?>
      <DIV class="content">
           </br></br>
            <?php 
            BuildMessageSet('Message');
            if(isset($_SESSION['ManageMessage'])){

             //   echo'<div id="popAlert"><div id ="alert">One or more fields were left incomplete or invalid. Ensure all required fields have values and that these values are valid.</div></div></br></br>';
                 BuildContentHeader('Add New Group', "", "", false); 
                  echo'<div>
                     <p>A new group can be added using the form below.</p>
                     <BR /><BR />
                    <table align="center" class="standard">
                        <tr>
                              <td colspan="2" class="header">Add New Group
                              </td>
                        </tr>
                        <tr>
                              <td colspan="2">Enter the group name and then select the staff members
                              </td>
                        </tr>
                        <tr>
                            <td>
                             <label class="left" >Group Name: <span class ="note">*</span> </label>
                             </td>      
                             <td>
                            <input tabindex="1" value="" type="text" id = "Grouptxt"><span id="duplicate_name">Name already exist</span>
                             </td>  
                        </tr>
                        <tr>
                            <td>
                             <label class="left" >Staff: <span class ="note">*</span> </label>
                             </td>      
                             <td>';
                                 getActiveStaff(2, "Edit_contacts", "seltest Long"); 
                             echo'</td>  
                        </tr>  
                        <tr >
                            <td colspan="2"  class="right">
                                <INPUT id= "saveNewGroup" tabindex="3" name="Submit" type="button" class ="button" value="Save"/>
                            </td>
                           
                        </tr>
                    </table>';
                                   
                  BuildContentHeader('Delete a Group', "", "", false);
                  echo'<div>
                     <p>A group can be deleted using the form below</p>
                     <BR /><BR />
                     <table align="center" class="standard">
                      <FORM method="post" action="Handlers/Message_Handler.php">
                       <INPUT name="Type" type="hidden" value="DeleteGroup">
                       <tr>
                              <td colspan="2" class="header">Delete Group
                              </td>
                        </tr>
                        <tr>
                              <td colspan="2">Select the group and then click delete.
                              </td>
                        </tr>
                        <tr>
                            <td>
                             <label class="left" >Group Name: <span class ="note">*</span> </label>
                             </td>      
                             <td>';
                                getActiveStaffGroup_SMS("group", "Long seltest", "groupsel", array($_SESSION['ManageMessage'][0]),0,0);
                             echo'</td>  
                        </tr>
                       
                        <tr >
                            <td colspan="2"  class="right">
                               <INPUT id ="Delete" tabindex="5" name="Submit" type="Submit" class ="button" value="Delete"/>
                            </td>
                           
                        </tr>
                    </FORM> 
                    </table>

                    </div></br>'; 
                             
                      echo'<div id="backbuttonsection">
                     <FORM method="post" action="Handlers/Message_Handler.php">
                     <INPUT name="Type" type="hidden" value="ManageSessionEnd">
                     <INPUT id ="Manage" tabindex="5" name="Submit" type="Submit" class ="button" value="Back"/>
                    </FORM> 
                    </div>';  
                             
       
            }
            else if(isset($_SESSION['ViewMessage'])){
 
                BuildContentHeader('View And Edit Groups', "", "", false);          
                echo' <p>A group can be editted using the table below</p>
                <BR /><BR /><div id="gridContainer"></div>';
                
                  echo'<div class = "Buttonsection"">
                     <FORM method="post" action="Handlers/Message_Handler.php">
                     <INPUT name="Type" type="hidden" value="ManageSessionEnd">
                     <INPUT id ="Manage" tabindex="5" name="Submit" type="Submit" class ="button" value="Back"/>
                    </FORM> 
                    </div>';  
                  
                 
            }
            else{
                
               echo '<div id="ConnectionProb" class="error hide">Possible Connection Loss</div>';
               echo '<div id="insufficientCrt" class="error hide">Insufficient Credits</div>';
                
               BuildContentHeader('Send a Message', "", "", false);

               echo '<DIV class="contentflow">
                 <div class="Hide" id="tdAlert"><span class="closebtn" onclick="this.parentElement.style.display="none";">&times;</span> Sent Successfully</div>  
                <P>A message can be sent using the form below</P>
                <BR /><BR />
                <TABLE cellspacing="5" align="center" class="standard">
                            <TR>
                              <TD colspan="4" class="header">Add Recipient
                              </TD>
                            </TR>
                            <TR>
                              <TD colspan="4">Select either a staff member or a group as a recipient. Please note that "All staff" in groups refers to Intranet and Autonet staff.
                              </TD>
                            </TR>
                            <TR>
                              <TD class="short">Recipient:
                                <SPAN class="note">*
                                </SPAN>
                              </TD>
                              <TD>
                                 <span id="Individual">';   getActiveStaffSMS(2, "contacts", "seltest Long"); echo'</span>
                                 <span id="Group" class="Hide">';   getActiveStaffGroup_SMS("group", "selectDrop seltest", "groupsel", array("all"),0,1); echo'</span>
                              </TD>
                              <TD class="right">
                                <input id="Groups" type="checkbox" onchange="doalert(this)" text="Groups"><label for="Groups" class="label">Groups</label>    
                              </TD>
                            </TR>
                            <TR>
                              <TD colspan="4" class="header">Type Message
                              </TD>
                            </TR>
                            <TR>
                              <TD colspan="4"> 
                                    <textarea id="box" placeholder="Type message here"  tabindex="9" name="Comments" class="standard" maxlength="459"></textarea>
                              </TD>
                            </TR>
                            <TR>
                              <TD class="center">
                                <span id="Credit" class="Credit"></span>
                              </TD>
                              <TD class="center">
                                <span id="count"><b>Characters left:</b> 160. 1 SMS(s)</span>  
                              </TD>
                              <TD class="center">
                                <span id="NumberOfRecipients"><b>Recipients:</b> 0</span>
                              </TD>
                              <TD class="right">
                                <span><b><INPUT id ="Send" tabindex="7" name="Submit" type="button" class="button" value="Send to Staff"/></b></span>
                                <span><b><INPUT id ="SendGroup" tabindex="7" name="Submit" type="button" class="Hide" value="Send to Group"/></b></span>
                              </TD>
                            </TR>    
                </TABLE>
              </DIV>';

               BuildContentHeader('Manage Groups', "", "", false);

               echo '<DIV class="contentflow">
                <P>A group can be added or edited using the form below</P>
                <BR /><BR />
                <TABLE cellspacing="5" align="center" class="standard">
                   <FORM method="post" action="Handlers/Message_Handler.php">
                     <INPUT name="Type" type="hidden" value="ManageSession">
                            <TR>
                              <TD colspan="4" class="header">Manage
                              </TD>
                            </TR>
                            <TR>
                              <TD colspan="3"> Click Manage to add, or delete groups.
                              <TD class="right"> <INPUT id ="Manage" tabindex="5" name="Submit" type="Submit" class ="button" value="Manage"/>
                              </TD>                        
                            </TR>  
                              <TR>
                              <TD colspan="4" class="header">Edit
                              </TD>
                            </TR>
                            <TR>
                              <TD colspan="3">Click View to view or edit all groups.
                              <TD class="right"> <INPUT id ="View" tabindex="5" name="Submit" type="Submit" class ="button" value="Edit"/>
                              </TD>                        
                            </TR>  
                    </FORM>        
                </TABLE>
              </DIV>';               
            }              
                             
            ?>
          

     
      </DIV>
    </DIV>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
  

      echo'<script src="Scripts/DevExpress/js/datagrids.js" type="text/javascript"></script>';
 
      ?>     
      
    </BODY>
</HTML>


 