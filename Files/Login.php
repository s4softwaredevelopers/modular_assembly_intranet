<?php
include 'Scripts/Include.php';
SetSettings();
?>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
 <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Home');
      //////////////////////////////////////////////////////////////////////////
 ?>
</head>
<body>
<?php
// PHP SCRIPT ////////////////////////////////////////////////////////////
BuildBanner();
//////////////////////////////////////////////////////////////////////////
?>
<DIV class="contentcontainer"><?php
    // PHP SCRIPT //////////////////////////////////////////////////////////
    BuildMenu('Main', 'index.php');

    function GetColour($Group)
    {
        switch ($Group)
        {
            case 5:
                return 'sky';
                break;
            case 6:
                return 'rowA';
                break;
            case 8:
                return 'rowB';
                break;
            default;
                return "";
                break;
        }
    }
    ////////////////////////////////////////////////////////////////////////
    ?>
    <DIV class="content">
        <BR /><BR />
        <?php
        // PHP SCRIPT ////////////////////////////////////////////////////////
        BuildMessageSet('Home');
        //////////////////////////////////////////////////////////////////////
        ///
        BuildContentHeader('Login', "", "", false);
        echo '<BR style="line-height:100px;" />
                    <DIV class="contentflow">
                      <P>Use the login below to access the intranet and remember to logout once you have finished.</P>
                      <BR /><BR />
                      <DIV>
                        <TABLE cellspacing="5" align="center" class="short">
                          <FORM method="post" action="Handlers/Login.php">
                            <TR>
                              <TD colspan="2" class="header">Login Details
                              </TD>
                            </TR>
                            <TR>
                              <TD class="short">Username:
                                <SPAN class="note">*
                                </SPAN>
                              </TD>
                              <TD>
                                <INPUT tabindex="1" name="Username" type="text" class="text long" />
                              </TD>
                            </TR>
                            <TR>
                              <TD class="half">Password:                              
                                <SPAN class="note">*
                                </SPAN>
                              </TD>
                              <TD>
                                <INPUT tabindex="2" name="Password" type="password" class="text long" />
                              </TD>
                            </TR>
                            <TR>
                              <TD colspan="2" class="center">
                                <INPUT tabindex="3" type="submit" class="button" value="Login" />
                              </TD>
                            </TR>
                          </FORM>
                        </TABLE>
                      </DIV>
                    </DIV>
                    <DIV>
                      <BR />
                      <SPAN class="note">*
                      </SPAN>
                      These fields are required.
                      
                    </DIV>';

        ?>
</body>
</html>


