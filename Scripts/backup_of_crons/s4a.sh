#This script searches in the current folder for all the quotes and copy them to the correct folder on the s4autonet.
#!/bin/bash
clear
directory="." #Root Location to search 
copyLocation="/home/e-smith/files/ibays/s4autonet/html/Files/Links/Quotes" #Location of where the files should be copied to

browsefolders (){
  for i in "$1"/*; 
  do
    extension=`echo "$i" | cut -d'.' -f3`
    if [ "$extension" != "" ]
    then
      if echo "$i" | grep -q "15" ; then   
           if [ "$extension" == "pdf" ] || [ "$extension" == "xml" ] || [ "$extension" == "doc" ] || [ "$extension" == "docx" ] ;then  #File types to search for
 	      echo "Sync the folders...";
             cp "$i" "$copyLocation";
             echo "Sync completed";
             echo "";
          fi
      fi
    fi
    if [ -d "$i" ]; then  
    browsefolders "$i"
    fi
  done
}
browsefolders  "$directory"