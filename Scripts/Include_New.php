<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 14 March 2008                               //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This scripting page provides a central link to all scripting pages.      //
  //////////////////////////////////////////////////////////////////////////////
  
  include 'Auth_New.php';       //Authentication and authorisation.
  //include 'Database.php'; //Database access. Already declared in Graphing.php.
  //include 'Display.php';  //Controls page display and generates page elements. Already declared in Database.php.
  //include 'Graphing.php'; //Displays graphs and charts. Already declared in Imaging.php.
  include 'Imaging_New.php';    //Resizes images.
  //include 'Mailing.php';  //Sends mails. Already declared in Database.php.
  //include 'Settings.php'; //Controls caching and sessions. Already declared in Graphing.php.
?>