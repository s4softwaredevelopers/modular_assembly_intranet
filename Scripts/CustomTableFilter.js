/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var makeSubTableVisible = false;

function searchTable(inputVal) {
    var count = 0;
    var table = $('#parent_table');
    table.find('tr').each(function (index, row) {
        var allCells = $(row).find('td');
        if (allCells.length > 0) {
            var found = false;
            allCells.each(function (index, td) {
                var regExp = new RegExp(inputVal, 'i');
                if (regExp.test($(td).text())) {
                    found = true;
                    return false;
                }
            });

            if (found == true || makeSubTableVisible == true) {
                $(".parentRow img").attr("src", "Images/Bullet_toggle_plus.png");
                if ($(row).hasClass('nested')) {
                    $(row).prev('tr').show();

                } else {
                    $(row).show();
                }

                makeSubTableVisible = false;
                count = count + 1;
            } else{ $(row).hide(); $(".parentRow img").attr("src", "Images/Bullet_toggle_plus.png");}




            if (found == true){ makeSubTableVisible = true;}

        }
    });
    return count;
}

$('#Go').click(function () {
    $('#parent_table').find('.noRecords').remove();
    var rowCount = searchTable($('#search').val());
    // $("#parent_table").find(".nested").hide();
    $(".sub_table").find("tr").removeAttr('style');
    $("#parent_table").find(".nested").hide();

    $('#parent_table').find('.noRecords').remove();
    if (rowCount == 0) {
        $('#parent_table').append('<tr class="noRecords"><td colspan="9" class="header">No results matched</td></tr>')
    }

});

$(document).on("click", ".parentRow", function () {
    $(this).nextAll('.nested:first').toggle();
    
    var src = $(this).find("img").attr('src');
    if(src == "Images/Bullet_toggle_minus.png")
        $(this).find("img").attr("src", "Images/Bullet_toggle_plus.png")
    else
        $(this).find("img").attr("src", "Images/Bullet_toggle_minus.png")
     
});

$(document).on("click", "#collapseAll", function () {
     var linkname = $('#collapseAll a').text();
  
     var table = $('#parent_table');
        table.find('.parentRow:visible').each(function (index,row) {
     if(linkname == "Expand All")
     {
         $('#collapseAll a').text('Collapse All');
         $(".parentRow img").attr("src", "Images/Bullet_toggle_minus.png");
       
            $(row).next('tr').show();  
     }
      });
    if(linkname == "Collapse All")
     {
        $('#collapseAll a').text('Expand All');
        $(".parentRow img").attr("src", "Images/Bullet_toggle_plus.png");
        $('.nested').hide();
    }
  
});

$('.sub_table').tableFilter();