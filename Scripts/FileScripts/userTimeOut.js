$(document).ready(function () {
    
    $.ajax({
        url: "Handlers/LoginCheck.php",
        type: "GET",
        success: function(response)
        {
            //Only start the timer if user is logged in
            if (response === "LoggedIn") {
                /* popups a timeout dialog with 'Log out' and 'Stay Logged in' options after a certain idle time. If 'Log Out' is 
                clicked, the logout handler is called, the logout handler unsets the sessions,the page is redirected to the index page.
                If 'Stay Logged in' is clicked, a keep-alive URL is requested through AJAX. If no options is selected after another set amount of time, the page is automatically redirected to a timeout URL*/
                    $(document).userTimeout({
                        session: 600000, //10min
                        logouturl:'index.php',
                        notify:true,
                        timer:true,
                        ui:'jqueryui'
                    });
            }

        },
        error: function(xhr)
        {
            console.log(xhr);
        }
    });

});
