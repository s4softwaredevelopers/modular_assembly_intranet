/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {     
            var user;
            var locations;
            var editing = false;


           $.ajax({
                url: "Handlers/Phonelist_Handler.php", //refer to the file
                type: "POST", //send it through get method            
                dataType: "json",
                data: { //arguments
                   Location: "" 
                },
                success: function(response) 
                {
                 
                  user = response[0];
                  locations = response[1];
                   console.log(user);
                   
                   console.log(locations);
                   
                   // make dx datagrid
           $.ajax({
                url: "Handlers/Phonelist_Handler.php", //refer to the file
                type: "POST", //send it through get method            
                data: { //arguments
                 Phonelist:"phonelist"
                },
                dataType: "json",
                success:function(response){
                   console.log(response);
                   if(response[0] === "390" || response[0] === "231" || response[0] === "282" || response[0] === "426"|| response[0] === "405" || response[0] === "470"){
                       editing = true;
                   }
                   
                     $("#gridContainer").dxDataGrid({
                        dataSource: response[1],
                        grouping: {
                            autoExpandAll: true
                        },
                        export:{
                            enabled: true,
                            fileName: "Phonelist"
                        },
                        searchPanel: {
                            visible: true,
                            width: 240,
                            placeholder: "Search..."
                        }, paging: { enabled: false },
                        editing: {
                            mode: "form",
                            allowUpdating: editing,
                            allowDeleting: editing,
                            allowAdding: editing     
                        },
                        groupPanel: {
                          visible: true
                        },
                        columns: 
                        [
                            { dataField: 'Staff_member', caption: 'Staff Member', validationRules: [{
                                          type: "required",
                                          message: "Please, insert a Staff member!"
                                        }] },
                            { dataField: 'Extension', width: 155 , alignment : "center" },
                            { dataField: 'Tel', caption: 'Tell', width: 200 , alignment : "center" },
                            { dataField: 'Cel', caption: 'Cell',width: 200 , alignment : "center"},
                            { dataField: 'Location',groupIndex: 0, lookup: { dataSource: locations, valueExpr: 'Location', displayExpr: 'Location' },  validationRules: [{
                                          type: "required",
                                          message: "Please, insert a Location!"
                                        }]},
                            { dataField: 'Floor', caption: 'Floor',width: 100 , alignment : "center"}
                        ],
                         onContentReady:function(e){
                             var toolbar = e.element.find('.dx-datagrid-header-panel');
                             toolbar.append('<div id="btnExportPDF"></div>');

                             $('#btnExportPDF').dxButton({
                                     icon:'export',
                                     hint:'View as PDF',
                                     text:'PDF',
                                 onClick:function (e) {
                                     var FileN = 'Phonelist';
                                     GenerateDocument('POST', 'Handlers/PhonelistExport.php', {
                                         SQlArray: response[1],
                                         FileName: FileN}, '_blank');
                                 }
                         });
                         },
                        onRowRemoving: function(info)
                        {
                           $.ajax({
                                    url: "Handlers/Phonelist_Handler.php", //refer to the file
                                    type: "POST", //send it through get method            
                                    data: { //arguments
                                        Delete: info.data.Id  
                                    },
                                    success: function(response) { 
                                        console.log(response);
                                    },
                                    error: function(xhr) {
                                        var locations = [];
                                        console.log(xhr);
                                    }
                                });   
                        },  
                        
                        onRowInserted: function(e) {
                            var memberi  = ""; 
                            var locationi = "";   
                            var  extentioni = "";  
                            var teli = "";  
                            var celi = "";   
   
                            if(e.data.Staff_member !== undefined){        
                                memberi = e.data.Staff_member;
                            }
                            if(e.data.Location !== undefined){     
                               locationi = e.data.Location;
                            }
                            if(e.data.Extension !== undefined){
                                extentioni = e.data.Extension;
                            }
                            if(e.data.Tel !== undefined){
                                teli = e.data.Tel;
                            }
                            if(e.data.Cel !== undefined){
                                celi = e.data.Cel;
                            }    
                            
                            console.log(e.data);      
                            console.log(memberi + extentioni + teli + celi + locationi);
                           
                            $.ajax({
                                url: "Handlers/Phonelist_Handler.php", //refer to the file
                                type: "POST", //send it through get method            
                                dataType: "json",
                                data: { 
                                   member_insert: memberi,
                                   extention_insert: extentioni,
                                   tel_insert: teli,
                                   cel_insert: celi,
                                   location_insert: locationi
                                },

                                success:function(response){
                                    var dataGrid = $('#gridContainer').dxDataGrid('instance');
                                    dataGrid.option('dataSource',response);

                                },
                                error: function(xhr) {
                                   console.log(xhr);
                                 }
                             });
                        },   
                        onRowUpdated: function(e) {    
                            
                            console.log(e);
                            var id = e.key.Id;
                            var memberU = e.key.Staff_member;
                            var extentionU = e.key.Extension;
                            var telU = e.key.Tel;
                            var celU = e.key.Cel;
                            var locationU = e.key.Location;

                            console.log( id + memberU+
                                extentionU +
                              telU +
                            celU +
                             locationU );
                            
                               $.ajax({
                                    url: "Handlers/Phonelist_Handler.php", //refer to the file
                                    type: "POST", //send it through get method            
                                    dataType: "json",
                                    data: { //arguments
                                       Id_update: id,
                                       member_update: memberU,
                                       extention_update:extentionU,
                                       cel_update: celU,
                                       tel_update: telU,
                                       location_update: locationU
                                    },

                                    success:function(response){       
                                        
                                           var dataGrid = $('#gridContainer').dxDataGrid('instance');
                                           dataGrid.option('dataSource',response);
                                    },
                                    error: function(xhr) {
                                       console.log(xhr);
                                     }
                                 });             
                            }  
                    });
                },
                error: function(xhr) {
                   console.log(xhr);
                 }
            });
                  
                },
                error: function(xhr) 
                {
                  console.log(xhr);
                }
            });

    function GenerateDocument(verb, url, data, target) {
        var form = document.createElement("form");
        form.action = url;
        form.method = verb;
        form.target = target || "_self";
        if (data) {
            for (var key in data) {
                var input = document.createElement("textarea");
                input.name = key;
                input.value = typeof data[key] === "object" ? JSON.stringify(data[key]) : data[key];
                form.appendChild(input);
                console.log(data[key]);
            }
        }
        form.style.display = "none";
        document.body.appendChild(form);
        form.submit();

    };
            
        });

