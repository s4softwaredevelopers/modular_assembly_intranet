
 var savedone = false; 
 var span = null;
 var OvertimeList;
 var StaffCode_Overtime = "";
 var listErrors = [];  
 
 $(document).ready(function() {  
     $(".seltest").chosen({
			no_results_text: "No results matched"
		});
     
       $('input[value=Submit]').removeClass('wait');
     
     $('input[value=Submit]').on('click', function(){
         $('input[value=Submit]').addClass('wait');
     });
     
    
     
    var cusheight = $(window).height() -254;
   
    StaffCode_Overtime = $('#overtimeApproveGrid').attr('data-staffcode');
    
    
               
   if($('#overtimeApproveGrid').length){
        $.ajax({
        url: "Handlers/Overtime_Handler.php", 
        type: "GET",    
        data: { 
            function:"buildOvertimedatagrid",
            staff_code: StaffCode_Overtime
        },
        dataType: "json",
        success: function(response) {
            OvertimeList = response[0];
            console.log(OvertimeList);
            $("#overtimeApproveGrid").dxDataGrid({
                dataSource: response[0],
                allowColumnResizing: false,
                paging: {
                    enabled: false
                },
                height: cusheight,

               editing:{
                   allowUpdating: true,
                   mode: "batch"
               },
                columns: [{dataField:"Period", allowEditing: false,width:260}, 
                    {dataField: "OvertimePreApp_Hours",width:55, caption: "Hours",allowEditing: false, encodeHtml: false, calculateDisplayValue: function(e) { 
                            
                            if(e.Duplicate === "No hours"){
                                 return e.OvertimePreApp_Hours; 
                            }
                            else if(e.Duplicate[2]){  // is a error
                               return '<div class="Hours">'+ e.OvertimePreApp_Hours+  '<span class="tooltiptext">'+e.Duplicate[1]+'</span> </div>';  
                            }
                            else{
                                return e.OvertimePreApp_Hours; 
                            }
                           
                        }},
                    {dataField: "timesheetHrs", caption: "Hours on Timesheet"},
                    {dataField: "Project_Description", caption: "Project",allowEditing: false},
                    {dataField: "personAuthorized", caption: "Authorized By",allowEditing: false},
                    {dataField: "OvertimePreApp_Date_Log", width:125, caption: "Date Logged",allowEditing: false},  // 
                    {dataField: "OvertimePreApp_Description", caption: "Description",allowEditing: false}, 
                    {dataField: "Overtime" ,groupIndex: 0, allowEditing: false, width:120, calculateDisplayValue: function(e){
                            if(e.Overtime == 0){
                                return "Requested To Other";                
                            }
                            else if(e.Overtime == 1){
                                return "Requested";
                            }
                            else {
                                return "Approved "+ response[3] ;
                            }
                            
                    } }, 
                    {width:60, allowEditing: false, alignment: "center" ,  caption: "Details", dataType:"String", encodeHtml: false, calculateDisplayValue: function(e) { return "<span class='showDetail' data-OvertimeSd='"+ e.OvertimePreApp_Start +"' data-OvertimeEd='"+ e.OvertimePreApp_End +"'   data-OvertimeId='"+ e.OvertimePreApp_ID +"'> show </span>"; } },
                    {width:60,  dataField: "Approve", dataType:"boolean", value: true },
                    {width:60,dataField: "Deny", dataType:"boolean", value: false },
                    {dataField:"Reason",allowEditing: true,width:100},
                    {caption:"",allowEditing: false, allowResizing:false, width:20}
                 ],
                 masterDetail: {
                    
                        enabled: true,
                        height: "auto",
                      
                        template: function(container, options) { 

                        var currentOvertime = options.data;
                        var currentOvertimeLength = currentOvertime.OtherOvertime;
                      //  console.log(currentOvertimeLength.length);
                            container.addClass("internal-grid-container");
                            
                            if(currentOvertimeLength.length >0){
                             $("<div class='black'>").text("Related Overtime:").appendTo(container);    
                                 $("<div>")
                                .addClass("internal-grid")
                                .dxDataGrid({
                                    columnAutoWidth: true,
                                    allowColumnResizing: true,
                                    editing:{
                                        allowUpdating: false
                                    },
                                    noDataText: "No overtime",
                                    columns: [
                                        {dataField:"Staff"}, 
                                        {dataField:"Period",width:260}, 
                                        {dataField: "OvertimePreApp_Hours", caption: "Hours"},
                                        {dataField: "OvertimePreApp_Description", caption: "Description"}
                                    ],
                                    wordWrapEnabled: true,
                                    dataSource: currentOvertime.OtherOvertime,   
                                }).appendTo(container);
                            }
                            else{
                                $("<div class='black'>").text("No Related Overtime.").appendTo(container);
                            }
                           
                        }

                    },
                onCellPrepared: function(info){
                    if ( info.rowType === "data" && (info.columnIndex === 7 || info.columnIndex === 8 || info.columnIndex === 9)) {
                        if(info.key.Overtime !== 1){  
                            info.cellElement[0].innerHTML = "";
                        }
                    }
                      
                     if (info.column.caption == "Approve") {
				info.cellElement.addClass("checkbox1");
                    	}
                  
                    if (info.column.caption == "Deny") {
				info.cellElement.addClass("checkbox2");
			}  
                    
                    
                },
                wordWrapEnabled: true,
                onCellClick:function(info){
                    
                    if (info.column.caption == "Approve") {
				if (info.value == true) {
					info.cellElement.closest("tr").find(".checkbox2 .dx-checkbox").dxCheckBox("instance").option("value", false);
				} else if ( info.value == false ) {
					info.cellElement.closest("tr").find(".checkbox2 .dx-checkbox").dxCheckBox("instance").option("value", false);
				}
			}
			
			if (info.column.caption == "Deny") {
				if (info.value == true) {
					info.cellElement.closest("tr").find(".checkbox1 .dx-checkbox").dxCheckBox("instance").option("value", false);
				} else if ( info.value == false ) {
					info.cellElement.closest("tr").find(".checkbox1 .dx-checkbox").dxCheckBox("instance").option("value", false);
				}
			}
                        
                },                
                onRowPrepared: function(info){                    
                    if ( info.rowType === "data" ){
                        info.rowElement.addClass("duplicate"+info.key.OvertimePreApp_ID);  // add class duplicateid
                        if(info.key.Overtime == 1 ){
                            
                            if(info.data.Duplicate[0] != parseFloat(info.data.OvertimePreApp_Hours)){
                                info.rowElement.css("background-color", "rgba(255, 168, 0, 0.61)");
                            }
                            
                            var isRed = false;
                            // match current with any other
                            OvertimeList.forEach(function(element){
                                
                                var start = element.OvertimePreApp_Start; //s
                                var end = element.OvertimePreApp_End;
                                var dateRequestedStart = info.key.OvertimePreApp_Start;
                                var dateRequestedEnd = info.key.OvertimePreApp_End; 
                                    if(info.key.OvertimePreApp_ID != element.OvertimePreApp_ID){
                                        if(start <  dateRequestedStart && end > dateRequestedStart  ){
                                            info.rowElement.css("background-color", "rgba(255, 0, 0, 0.32)");
                                            isRed = true;
                                         }

                                        else if(start >= dateRequestedStart && end <= dateRequestedEnd){
                                            info.rowElement.css("background-color", "rgba(255, 0, 0, 0.32)");
                                            isRed = true;
                                        }

                                        else if(start >= dateRequestedStart && start < dateRequestedEnd){
                                            info.rowElement.css("background-color", "rgba(255, 0, 0, 0.32)");
                                            isRed = true;
                                        }

                                    }
                            });
                            
                            if(!isRed){
                                info.rowElement.find('span.showDetail').hide();
                            }
                        }
                    }
                },
                onRowUpdated: function(row){ 
                    var Approve = row.key.Approve;
                    var Deny = row.key.Deny;
                    var ID = row.key.OvertimePreApp_ID;
                    var Reason = row.key.Reason;
                    var status = null;

                    if(Approve && !Deny){  // aprove true and deny false
                         status = 'Approve';
                    }

                    else if(!Approve && Deny){  // aprove true and deny false
                         status = 'Deny';
                    }
                    
                    if(status!= null){
                       //add dialog
                        $.ajax({
                            url: "Handlers/Overtime_Handler.php", //refer to the file
                            type: "GET", //send it through get method            
                            dataType: "json",
                            data: { //arguments
                                function: 'SaveOvertimeMulti',
                                OvertimeId: ID,
                                status: status,
                                Reason: Reason,
                                staffCode: StaffCode_Overtime,
                                Multi: row.key.OvertimePreApp_Multi
                               
                            },

                            success: function(response) { //if returns what must happen
                                console.log(response);
                                 var dataGrid = $('#overtimeApproveGrid').dxDataGrid('instance');
                                 dataGrid.option('dataSource',response[0][0]);
                                 
                                if(response[1]!=="pass"){
                                    console.log("response has failed " + response[1] +"!== pass"  );
                                    listErrors.push(ID);
                                }
                                else{
                                     savedone = true;
                                }
                               
                            },

                            error: function(xhr) {
                                console.log(xhr);
                            }
                        });     
                    }
  
                },
                onContentReady: function(info){
                    
                   //console.log("in context realdy");
                   if(listErrors.length != 0){
                        console.log("ERRORS");
                        $('#ERRERS').html("Requested operation failed. An error occurred during the submission process.")
                        $('#ERRERS').removeClass('notify');
                        $('#ERRERS').addClass('error');
                        $('#ERRERS').show();
                       console.log(listErrors);
                       listErrors = [];
                       savedone = false;
                   }
                   else if(savedone){
                         $('#ERRERS').html("Requested operation has completed successfully.")
                         $('#ERRERS').removeClass('error');
                          $('#ERRERS').addClass('notify');
                         $('#ERRERS').show();
                         savedone = false;
                   }
                   else{
                        $('#ERRERS').hide();
                   }
                    
                   $(".showDetail").on('click',function(){ 
                          span = $(this);
                          var dataGrid = $('#overtimeApproveGrid').dxDataGrid('instance');
                         dataGrid.expandAll();               
                   });
     
                   if(span != null){
                       doHide(); 
                   }
                }
            });
        },
        error: function(xhr) {
            console.log(xhr);
        }
    });   
    }
 });
    
    

   
 
function doHide(){
   $('.hidethedata').removeClass('hidethedata');
   $('.highlight').removeClass('highlight');
   var state =  span.text();
   
   if(state === ' show '){

        var Curovertimeid = span.context.dataset.overtimeid;
        $('.dx-row.dx-data-row.dx-column-lines.duplicate'+Curovertimeid).addClass('highlight');
        $('.showDetail').hide();
        $('.dx-row.dx-data-row.dx-column-lines.duplicate'+Curovertimeid).find('.showDetail').show();
        $('.dx-row.dx-data-row.dx-column-lines.duplicate'+Curovertimeid).find('.showDetail').text('hide');

        var startDate = span.context.dataset.overtimesd;
        var endDate = span.context.dataset.overtimeed;

        var classes =  duplicateEntry(startDate,endDate,Curovertimeid);
        console.log(classes);      
        if(classes!== ""){  
           $(classes).addClass("hidethedata");
            $('.showDetail').show();
        }             
    }
   else {
        span.text('show');
   }
}


function duplicateEntry(dateRequestedStart,dateRequestedEnd, RequestedID  ){
        var classes = "";
        var notO = "";
        OvertimeList.forEach(function(element){
            var start = element.OvertimePreApp_Start;
            var end = element.OvertimePreApp_End;
         
            if(RequestedID != element.OvertimePreApp_ID){      
               
                if(start <  dateRequestedStart && end > dateRequestedStart  ){
                   classes =  classes + ".duplicate" + element.OvertimePreApp_ID  + ", "; 
                }

                else if(start >= dateRequestedStart && end <= dateRequestedEnd){
                     classes =  classes + ".duplicate" + element.OvertimePreApp_ID  + ", "; 
                }

                else if(start >= dateRequestedStart && start < dateRequestedEnd){
                    classes =  classes + ".duplicate" + element.OvertimePreApp_ID  + ", "; 
                }   
                else{
                     notO =  notO + ".duplicate" + element.OvertimePreApp_ID  + ", "; 
                }
            }
        
         });
        return notO.slice(0, -2); 
}

