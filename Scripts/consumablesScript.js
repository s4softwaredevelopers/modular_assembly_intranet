/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

  //**************************************************************************
            //NEEDS TO BE COPIED TO INTRANET JAVASCRIPT FILE
  $(document).ready(function() 
{
    $('#btnAddConsumble').click( function()
    {
        $.get("mobile.php?pid=consumable&fid=add&name="+$('[name=tbName]').val()+"&description="+$('[name=tbdescription]').val()+"&detail="+$('[name=tbDetail]').val()+"&minQty="+$('[name=minQty]').val()+"&maxQty="+$('[name=maxQty]').val()+"&qtyInStore="+$('[name=qtyInStore]').val(), function(response)
        {
            alert(response);
            location.reload();
        });
    });
    
    $('#btnUpdateConsumble').click( function()
    {
        var id = $(this).closest('tr').find('[name=id]').val();
        var name = $(this).closest('tr').find('[name=name]').val(); 
        var description = $(this).closest('tr').find('[name=description]').val();
        var details = $(this).closest('tr').find('[name=details]').val();
        var minQty = $(this).closest('tr').find('[name=minQty]').val();
        var maxQty = $(this).closest('tr').find('[name=maxQty]').val();
        var qtyInStore = $(this).closest('tr').find('[name=qtyInStore]').val();
        $.get("mobile.php?pid=consumable&fid=update&id="+id+"&name="+name+"&description="+description+"&detail="+details+"&minQty="+minQty+"&maxQty="+maxQty+"&qtyInStore="+qtyInStore, function(response)
        {
            alert(response);
            location.reload();
        });
    });
    
    $('#btnDeleteConsumble').click( function()
    {
        var id = $(this).closest('tr').find('[name=id]').val();
        $.get("mobile.php?pid=consumable&fid=delete&id="+id, function(response)
        {
            alert(response);
            location.reload();
        });
    });
    });