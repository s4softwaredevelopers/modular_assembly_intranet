var custOptions = "", 
    taskOptions = "",
    projOptions ="",
    activityOptions = "",
    completionOptions = "";
$(document).ready(function()
{
    // variable to hold request
    var request;
    // bind to the submit event of our form
    $("#foo").submit(function(event){
        
        // abort any pending request
        if (request) {
            request.abort();
        }
        // setup some local variables
        var $form = $(this);
        // let's select and cache all the fields
        var $inputs = $form.find("input, select, button, textarea");
        // serialize the data in the form
        var serializedData = $form.serialize();

        // let's disable the inputs for the duration of the ajax request
        $inputs.prop("disabled", true);

        // fire off the request to /form.php
        var request = $.ajax({
            url: "handle.php",
            type: "post",
            data: serializedData
        });

        // callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR){
            // log a message to the console
            console.log(response);
            $('#contentDiv').html(response);
        });

        // callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown){
            // log the error to the console
            console.error(
                "The following error occured: "+
                textStatus, errorThrown
            );
        });

        // callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            // reenable the inputs
            $inputs.prop("disabled", false);
        });

        // prevent default posting of form
        event.preventDefault();
    });

    $(".contentflow form").on("click", "#toggle-view table .button_",function(){
        $('.loading').show();
        $('.loading-text').show();
        var obj = $(this).closest("tr").find("td");
        var tr = $(this).closest("tr"),
        trId = $(tr).attr("id");
        var customerVal = $(obj[0]).find("select").chosen().val(),
        projectVal = $(obj[1]).find("select").chosen().val(),
        taskVal = $(obj[2]).find("select").chosen().val(),
        descriptionVal = $(obj[3]).find("input").val(),
        hoursVal = ($(this).val() == "Remove") ? 0 : parseFloat($(obj[4]).find("input").val()),
        rowNumber = ($(this).val() == "Add") ? 0 : parseInt(trId.match(/\d+/)),
        dayVal = $(obj[5]).html(),
        _that = this;
        custOptions = (custOptions != "" && custOptions != null) ? custOptions : $('#CustomerOpt').html();
        taskOptions = (taskOptions != "" && taskOptions != null)? taskOptions : $('#TaskOpt').html();
        projOptions = (projOptions != "" && projOptions != null) ? projOptions : $('#ProjectOpt').html();
        var _tbody = $(_that).closest("tbody");
        //days details
        var startMinute = $("select[name='StartMinute"+dayVal+"'").val(),
        startHour = $("select[name='StartHour"+dayVal+"']").val(),
        endMinute = $("select[name='EndMinute"+dayVal+"']").val(),
        endHour = $("select[name='EndHour"+dayVal+"']").val(),
        startD = startHour +""+ startMinute,
        endD = endHour +""+ endMinute,
        lunchTime = Math.round($("input[name = 'Lunch"+dayVal+"']").val()),
        hoursLogged = parseFloat($("input[name = 'hoursLogged"+dayVal+"']").val()) + hoursVal,
        hoursWorked = (((parseInt(endHour)*60) + parseInt(endMinute)) - ((parseInt(startHour)*60) + parseInt(startMinute)) - Math.round(lunchTime)) / 60;
        var objVal = {process:$(this).val(), cust:customerVal, proj:projectVal, task:taskVal, desc:descriptionVal, 
            hours:hoursVal, day:dayVal, hoursWorked:hoursWorked, hoursLogged:hoursLogged,startD:startD, endD:endD,
            lunchTime:lunchTime, rowNumber:rowNumber};
        $.post( "Handlers/S4timesheet_Handler.php", objVal, function(data){
            var response = $.parseJSON(data);
            if(response[0] == "Error")
            {
                $('.loading').hide();
                $('.loading-text').hide();
                $("#errors> .error").html(response[1]);
                $("#errors").show();
                $("div.content").animate({ scrollTop: 0 }, "slow");
            }
            else
            {
                $("#errors").hide();
                var itemPos = parseInt(response[0]);
                if(itemPos > 0 && $(_that).val() == "Add"){
                    html = '<TR id="tr'+itemPos+'" class="day'+dayVal+'">'+'<TD><SELECT tabindex="1" name="Customer'+itemPos+'" class="long_ seltest" id="Customer'+itemPos+'day'+dayVal+'">'+custOptions+'</SELECT></TD>'+
                            '<TD><SELECT tabindex="1" name="Project'+itemPos+'" class="long_ seltest" id="Project'+itemPos+'day'+dayVal+'">'+projOptions+'</SELECT></TD>'+
                            '<TD><SELECT tabindex="1" name="Task'+itemPos+'" class="long_ seltest" id="Task'+itemPos+'day'+dayVal+'">'+taskOptions+'</SELECT></TD>'+
                            '<TD class="center"><INPUT tabindex="1" name="Description'+itemPos+'" type="text" class="standard" maxlength="100" value="'+descriptionVal+'"/></TD>'+
                            '<TD class="center"><INPUT tabindex="1" name="Hours'+itemPos+'" type="text" step="0.01" class="veryshort" maxlength="5" value="'+hoursVal+'"/></TD>'+
                            '<td style="display:none;">'+dayVal+'</td>'+
                            '<TD colspan="1" class="center"><INPUT tabindex="1" name="Remove'+itemPos+'" type="button" class="button button_" value="Remove" /></TD>'+
                            '<TD colspan="1" class="center"><INPUT tabindex="1" name="Update'+itemPos+'" type="button" class="button button_" value="Update" /></TD>'+
                        '</TR>';
                    $(_tbody).append(html);
                    var sSelector = ".day"+dayVal+"#tr"+itemPos+" td";
                    var td = $(_tbody).find(sSelector);
                    $(td[0]).find("select").val(customerVal);
                    $(td[1]).find("select").val(projectVal);
                    $(td[2]).find("select").val(taskVal);
                    
                    $($(td[1]).find("select")).chained($(td[0]).find("select"));
                    $($(td[2]).find("select")).chained($(td[1]).find("select"));
                    $(sSelector + " .seltest").chosen({ no_results_text: "No results matched" });
                    $(sSelector+" .seltest").on('change', function(){
                         $(sSelector+' .seltest').trigger('liszt:updated');
                    });
                    
                    //clear fields
                    $(obj[0]).find("select").val('').trigger('liszt:updated');
                    $(obj[1]).find("select").val('').trigger('liszt:updated');
                    $(obj[2]).find("select").val('').trigger('liszt:updated');
                    $(obj[3]).find("input").val("");
                    $(obj[4]).find("input").val("");
                    
                    
                    //update main table hours
                    $("input[name = 'hoursWorked"+dayVal+"']").val(response[1]);
                    $(".center.hoursWorked"+dayVal).html(response[1]);
                    $("input[name = 'hoursLogged"+dayVal+"']").val(hoursLogged);
                    $(".center.hoursLogged"+dayVal).html(hoursLogged);
                    $("input[name='hoursRemaining"+dayVal+"']").val(response[2]);
                    $(".center.hoursRemaining"+dayVal).html(response[2]);
                    
                    if((response[2] == 0) && ($(".center.hoursLogged"+dayVal).hasClass("error")))
                    {
                        $(".center.hoursLogged"+dayVal).removeClass("error");
                        $(".center.hoursRemaining"+dayVal).removeClass("error");
                    }
                    else if((response[2] != 0) && (!$(".center.hoursLogged"+dayVal).hasClass("error")))
                    {
                        $(".center.hoursLogged"+dayVal).addClass("error");
                        $(".center.hoursRemaining"+dayVal).addClass("error");
                    }
                    $('.loading').hide();
                    $('.loading-text').hide();
                }
                else if(itemPos > 0 || (response[0] == "Removed"))
                {
                    //update main table hours
                    $("input[name = 'hoursWorked"+dayVal+"']").val(hoursWorked);
                    $(".center.hoursWorked"+dayVal).html(hoursWorked);
                    $("input[name = 'hoursLogged"+dayVal+"']").val(response[1]);
                    $(".center.hoursLogged"+dayVal).html(response[1]);
                    $("input[name='hoursRemaining"+dayVal+"']").val(response[2]);
                    $(".center.hoursRemaining"+dayVal).html(response[2]);
                    
                    if(response[0] == "Removed"){
                        $(tr).remove();
                        
                        $(_tbody).find('tr.day'+dayVal).each(function(index) {
                            var i = index + 1;
                            $(this).find('td:nth-child(1) select').attr({ name: "Customer"+i, id: "Customer"+i+"day"+dayVal });
                            $(this).find('td:nth-child(2) select').attr({ name: "Project"+i, id: "Project"+i+"day"+dayVal });
                            $(this).find('td:nth-child(3) select').attr({ name: "Task"+i, id: "Task"+i+"day"+dayVal });
                            $(this).find('td:nth-child(4) input').attr({ name: "Description"+i});
                            $(this).find('td:nth-child(5) input').attr({ name: "Hours"+i});
                            $(this).find('td:nth-child(7) input').attr({ name: "Remove"+i});
                            $(this).find('td:nth-child(8) input').attr({ name: "Update"+i});
                            $(this).attr({ id: "tr"+i});
                         });
                         $('tr.day'+dayVal+' .seltest').show().removeClass('chzn-done');
                         $('tr.day'+dayVal+' .seltest').next().remove();
                         $('tr.day'+dayVal+' .seltest').chosen({ no_results_text: "No results matched" });
                    }
                    
                    if((response[2] == 0) && ($(".center.hoursLogged"+dayVal).hasClass("error")))
                    {
                        $(".center.hoursLogged"+dayVal).removeClass("error");
                        $(".center.hoursRemaining"+dayVal).removeClass("error");
                    }
                    else if((response[2] != 0) && (!$(".center.hoursLogged"+dayVal).hasClass("error")))
                    {
                        $(".center.hoursLogged"+dayVal).addClass("error");
                        $(".center.hoursRemaining"+dayVal).addClass("error");
                    }
                    $('.loading').hide();
                    $('.loading-text').hide();
                }
           }
            //$(".seltest").chosen({ no_results_text: "No results matched" });
        })
        .fail(function() {
          $('.loading').hide();
          $('.loading-text').hide();
          alert( "error" );
        });
    });
    

    $('#mainTable').on("click", 'input[value="Proceed"]', function(){
        $('.loading').show();
        $('.loading-text').show();
        var timeArray = new Array();
        var passed = true;
        $('#mainTable > tbody  > tr.dayRows').each(function(){
              var startHour = $(this).find('td:nth-child(2) select.hour').val(), 
                    startMinute = $(this).find('td:nth-child(2) select.minute').val(),
                    endHour = $(this).find('td:nth-child(3) select.hour').val(), 
                    endMinute = $(this).find('td:nth-child(3) select.minute').val(),
                    validLunch = $(this).find('td:nth-child(4) input').val(),
                    lunchTime = Math.round(validLunch),
                    today = new Date($(this).find('td:nth-child(1)').html());
              var startD = startHour+""+startMinute,
                    endD = endHour+""+endMinute;
              var validTimeStart = new Date(today.getFullYear(), today.getMonth(), today.getDate(), startHour, startMinute, 0).getTime()/1000;
              var validTimeEnd = new Date(today.getFullYear(), today.getMonth(), today.getDate(), endHour, endMinute, 0).getTime()/1000;
              

              if(isNaN(parseInt(validLunch)) || lunchTime < 0 || (startD+':00' == endD+':00') || (validTimeStart > validTimeEnd)){
                  $('.loading').hide();
                  $('.loading-text').hide();
                  $("#errors> .error").html('One or more fields were left incomplete or invalid. Ensure all required fields have values and that these values are valid.');
                  $("#errors").show();
                  $("div.content").animate({ scrollTop: 0 }, "slow");
                  passed = false;
                  return false;
              }
              else{    
                $("#errors").hide();
                var hoursWorked = (((parseInt(endHour)*60) + parseInt(endMinute)) - ((parseInt(startHour)*60) + parseInt(startMinute)) - lunchTime) / 60;
                timeArray.push(new Array(startD, endD, lunchTime, hoursWorked, hoursWorked, 0, $(this).find('td:nth-child(1)').html()));
              } 
        });
        if(passed){
            if($(this).hasClass('dsa'))
            {
                var htmlContent = BuildHtmlDSA(timeArray);

                $.post( "Handlers/TimesheetBasicDSANew_Handler.php", {process:$(this).val(),timeArray:timeArray}, function(data){
                    var response = $.parseJSON(data);
                    console.log(response);
                    if(timeArray.length == response.length)
                    {
                        $('.contentflow').find('#dsaTasksTable').prepend(htmlContent);
                        $('#dsaTasksTable').show();
                        $("#toggle-view > li > .panel > table > tbody > tr:first-child").each(function(i) {
                            var trs = $('#mainTable').find('tr.dayRows')[i];
                            $(trs).find('td:nth-child(5)').html(response[i][3]);
                            $(trs).find('input[name="hoursWorked'+(i+1)+'"]').val(response[i][3]);
                            $(trs).find('.center.hoursRemaining'+(i+1)).html(response[i][3]);
                            $(trs).find('input[name="hoursRemaining'+(i+1)+'"]').val(response[i][3]);
                        });
                        $(" .seltest").chosen({ no_results_text: "No results matched" });
                        $(".seltest").on('change', function(){
                             $('.seltest').trigger('liszt:updated');
                        });
                        $('#proceedBtns').hide();
                        $('#submitBtns').show();
                        
                    }
                    $('.loading').hide();
                    $('.loading-text').hide();
                })
                .fail(function() {
                    $('.loading').hide();
                    $('.loading-text').hide();
                     alert( "error" );
                });
            }
            else{
                var htmlContent = BuildHtml(timeArray);

                $.post( "Handlers/S4timesheet_Handler.php", {process:$(this).val(),timeArray:timeArray}, function(data){
                    var response = $.parseJSON(data);
                    console.log(response);
                    if(timeArray.length == response.length)
                    {
                        $('.contentflow').find('form').append(htmlContent);
                        $("#toggle-view > li > .panel > table > tbody > tr:first-child").each(function(i) {
                            var cust = "#"+ $(this).find('td:nth-child(1) select').attr('id'),
                                proj = "#"+ $(this).find('td:nth-child(2) select').attr('id'),
                                task = "#"+ $(this).find('td:nth-child(3) select').attr('id');
                            $(proj).chained(cust);
                            $(task).chained(proj);

                            var trs = $('#mainTable').find('tr.dayRows')[i];
                            $(trs).find('td:nth-child(5)').html(response[i][3]);
                            $(trs).find('input[name="hoursWorked'+(i+1)+'"]').val(response[i][3]);
                            $(trs).find('.center.hoursRemaining'+(i+1)).html(response[i][3]);
                            $(trs).find('input[name="hoursRemaining'+(i+1)+'"]').val(response[i][3]);
                        });
                        $(" .seltest").chosen({ no_results_text: "No results matched" });
                        $(".seltest").on('change', function(){
                             $('.seltest').trigger('liszt:updated');
                        });
                        $('#proceedBtns').hide();
                        $('#submitBtns').show();
                    }
                    $('.loading').hide();
                    $('.loading-text').hide();
                })
                .fail(function() {
                    $('.loading').hide();
                    $('.loading-text').hide();
                    
                     alert( "error" );
                });
            }
        }
    });
    
    
    $(".contentflow form").on("click", "#toggle-view table .dsaButton",function(){
        $('.loading').show();
        $('.loading-text').show();
        var obj = $(this).closest("tr").find("td");
        var tr = $(this).closest("tr"),
        trId = $(tr).attr("id");
        var projectVal = $(obj[0]).find("input").val(),
        wPosVal = $(obj[1]).find("input").val(),
        activityVal = $(obj[2]).find("select").chosen().val(),
        descriptionVal = $(obj[3]).find("input").val(),
        commentVal = $(obj[4]).find("input").val(),
        hoursVal = ($(this).val() == "Remove") ? 0 : parseFloat($(obj[5]).find("input").val()),
        completionVal = $(obj[6]).find("select").val(),
        rowNumber = ($(this).val() == "Add") ? 0 : parseInt(trId.match(/\d+/)),
        dayVal = $(obj[7]).html(),
        _that = this;
        activityOptions = (activityOptions != "" && activityOptions != null) ? activityOptions : $('.activity1Opt').html();
        completionOptions = (completionOptions != "" && completionOptions != null)? completionOptions : $('select[name="completion1Opt"]').html();
        var _tbody = $(_that).closest("tbody");
        //days details
        var startMinute = $("select[name='StartMinute"+dayVal+"'").val(),
        startHour = $("select[name='StartHour"+dayVal+"']").val(),
        endMinute = $("select[name='EndMinute"+dayVal+"']").val(),
        endHour = $("select[name='EndHour"+dayVal+"']").val(),
        startD = startHour +""+ startMinute,
        endD = endHour +""+ endMinute,
        lunchTime = Math.round($("input[name = 'Lunch"+dayVal+"']").val()),
        hoursLogged = parseFloat($("input[name = 'hoursLogged"+dayVal+"']").val()) + hoursVal,
        hoursWorked = (((parseInt(endHour)*60) + parseInt(endMinute)) - ((parseInt(startHour)*60) + parseInt(startMinute)) - Math.round(lunchTime)) / 60;
        var objVal = {process:$(this).val(), Project:projectVal, WPos: wPosVal, Activity:activityVal, Description:descriptionVal, 
            Comment:commentVal, Hours:hoursVal,Completion:completionVal, day:dayVal, hoursWorked:hoursWorked, hoursLogged:hoursLogged,startD:startD, endD:endD,
            lunchTime:lunchTime, rowNumber:rowNumber};
        $.post( "Handlers/TimesheetBasicDSANew_Handler.php", objVal, function(data){
            var response = $.parseJSON(data);
            console.log(response);
            if(response[0] == "Error")
            {
                $('.loading').hide();
                $('.loading-text').hide();
                
                $("#errors> .error").html(response[1]);
                $("#errors").show();
                $("div.content").animate({ scrollTop: 0 }, "slow");
            }
            else
            {
                $("#errors").hide();
                var itemPos = parseInt(response[0]);
                if(itemPos > 0 && $(_that).val() == "Add"){
                    html = '<TR id="tr'+itemPos+'" class="day'+dayVal+'">'+'<TD class="center'+(((projectVal.length < 6) || isNaN(projectVal)) ? ' red' : '')+'"><INPUT tabindex="1" name="Project'+itemPos+'" type="text" class="veryshort" maxlength="8" id="Project'+itemPos+'day'+dayVal+'" value="'+projectVal+'" onkeyup="'+scheduleAutoComplete(this,dayVal,itemPos)+'"/></TD>'+
                            '<TD class="center'+(((wPosVal.length > 4) || isNaN(wPosVal)) ? ' red' : '')+'"><INPUT tabindex="1" name="WPos'+itemPos+'" class="veryshort" id="WPos'+itemPos+'day'+dayVal+'" type="text" maxlength="8" value="'+wPosVal+'" onkeyup="'+scheduleAutoComplete(this,dayVal,itemPos)+'"/></TD>'+
                            '<TD class="'+((activityVal == 110 || activityVal == 120 || activityVal == 140 || activityVal == 185 || activityVal == 910 || activityVal == 920) ? '' : ' red')+'"><SELECT tabindex="1" name="Activity'+itemPos+'" class="standard seltest" id="Activity'+itemPos+'day'+dayVal+'">'+activityOptions+'</SELECT></TD>'+
                            '<TD class="center"><INPUT tabindex="1" name="Description'+itemPos+'" type="text" class="standard" maxlength="100" value="'+descriptionVal+'"/></TD>'+
                            '<TD class="center"><INPUT tabindex="1" name="Comment'+itemPos+'" type="text" class="standard" maxlength="100" value="'+commentVal+'"/></TD>'+
                            '<TD class="center"><INPUT tabindex="1" name="Hours'+itemPos+'" type="text" step="0.01" class="veryshort" maxlength="5" value="'+hoursVal+'"/></TD>'+
                            '<TD class="center bold"><SELECT tabindex="1" name="Completion'+itemPos+'" class="veryshort" id="Completion'+itemPos+'day'+dayVal+'">'+completionOptions+'</SELECT>%</TD>'+
                            '<td style="display:none;">'+dayVal+'</td>'+
                            '<TD colspan="1" class="center"><INPUT tabindex="1" name="Remove'+itemPos+'" type="button" class="button dsaButton" value="Remove" /></TD>'+
                            '<TD colspan="1" class="center"><INPUT tabindex="1" name="Update'+itemPos+'" type="button" class="button dsaButton" value="Update" /></TD>'+
                        '</TR>';
                    $(_tbody).append(html);
                    var sSelector = ".day"+dayVal+"#tr"+itemPos+" td";
                    var td = $(_tbody).find(sSelector);
                    $(td[2]).find("select").val(activityVal);
                    $(td[6]).find("select").val(completionVal);
                    $(sSelector + " .seltest").chosen({ no_results_text: "No results matched" });
                    $(sSelector+" .seltest").on('change', function(){ $(sSelector+' .seltest').trigger('liszt:updated'); });
                    
                    //clear fields
                    $(obj[0]).find("input").val('');
                    $(obj[1]).find("input").val("");
                    $(obj[2]).find("select").val("").trigger('liszt:updated');
                    $(obj[3]).find("input").val("");
                    $(obj[4]).find("input").val("");
                    $(obj[5]).find("input").val("");
                    $(obj[6]).find("select").val("");
                    
                    
                    //update main table hours
                    $("input[name = 'hoursWorked"+dayVal+"']").val(response[1]);
                    $(".center.hoursWorked"+dayVal).html(response[1]);
                    $("input[name = 'hoursLogged"+dayVal+"']").val(hoursLogged);
                    $(".center.hoursLogged"+dayVal).html(hoursLogged);
                    $("input[name='hoursRemaining"+dayVal+"']").val(response[2]);
                    $(".center.hoursRemaining"+dayVal).html(response[2]);
                    
                    if((response[2] == 0) && ($(".center.hoursLogged"+dayVal).hasClass("error")))
                    {
                        $(".center.hoursLogged"+dayVal).removeClass("error");
                        $(".center.hoursRemaining"+dayVal).removeClass("error");
                    }
                    else if((response[2] != 0) && (!$(".center.hoursLogged"+dayVal).hasClass("error")))
                    {
                        $(".center.hoursLogged"+dayVal).addClass("error");
                        $(".center.hoursRemaining"+dayVal).addClass("error");
                    }
                    $('.loading').hide();
                    $('.loading-text').hide();
                }
                else if(itemPos > 0 || (response[0] == "Removed"))
                {
                    if((projectVal.length < 6) || isNaN(projectVal))
                         $(obj[0]).addClass('red');
                    else 
                        $(obj[0]).removeClass('red');
                    
                    
                    if((wPosVal.length > 4) || isNaN(wPosVal))
                         $(obj[1]).addClass('red');
                    else
                        $(obj[1]).removeClass('red');
                    
                    if(((activityVal == 110 || activityVal == 120 || activityVal == 140 || activityVal == 185 || activityVal == 910 || activityVal == 920)))
                         $(obj[2]).removeClass('red');
                    else 
                        $(obj[2]).addClass('red');
                        
                    //update main table hours
                    $("input[name = 'hoursWorked"+dayVal+"']").val(hoursWorked);
                    $(".center.hoursWorked"+dayVal).html(hoursWorked);
                    $("input[name = 'hoursLogged"+dayVal+"']").val(response[1]);
                    $(".center.hoursLogged"+dayVal).html(response[1]);
                    $("input[name='hoursRemaining"+dayVal+"']").val(response[2]);
                    $(".center.hoursRemaining"+dayVal).html(response[2]);
                    
                    if(response[0] == "Removed"){
                        $(tr).remove();
                        $(_tbody).find('tr.day'+dayVal).each(function(index) {
                            var i = index + 1;
                            $(this).find('td:nth-child(1) input').attr({ name: "Project"+i});
                            $(this).find('td:nth-child(2) input').attr({ name: "WPos"+i });
                            $(this).find('td:nth-child(3) select').attr({ name: "Activity"+i});
                            $(this).find('td:nth-child(4) input').attr({ name: "Description"+i});
                            $(this).find('td:nth-child(5) input').attr({ name: "Comment"+i});
                            $(this).find('td:nth-child(6) input').attr({ name: "Hours"+i});
                            $(this).find('td:nth-child(7) select').attr({ name: "Completion"+i});
                            $(this).find('td:nth-child(9) input').attr({ name: "Remove"+i});
                            $(this).find('td:nth-child(10) input').attr({ name: "Update"+i});
                            $(this).attr({ id: "tr"+i});
                         });
                         $('tr.day'+dayVal+' .seltest').show().removeClass('chzn-done');
                         $('tr.day'+dayVal+' .seltest').next().remove();
                         $('tr.day'+dayVal+' .seltest').chosen({ no_results_text: "No results matched" });
                    }
                    
                    if((response[2] == 0) && ($(".center.hoursLogged"+dayVal).hasClass("error")))
                    {
                        $(".center.hoursLogged"+dayVal).removeClass("error");
                        $(".center.hoursRemaining"+dayVal).removeClass("error");
                    }
                    else if((response[2] != 0) && (!$(".center.hoursLogged"+dayVal).hasClass("error")))
                    {
                        $(".center.hoursLogged"+dayVal).addClass("error");
                        $(".center.hoursRemaining"+dayVal).addClass("error");
                    }
                    $('.loading').hide();
                    $('.loading-text').hide();
                }
           }
            //$(".seltest").chosen({ no_results_text: "No results matched" });
        })
        .fail(function() {
            $('.loading').hide();
             $('.loading-text').hide();
          alert( "error" );
        });
    });
    
});
function BuildHtml(itemsArray){
    custOptions = (custOptions != "" && custOptions != null) ? custOptions : $('#CustomerOpt').html();
    taskOptions = (taskOptions != "" && taskOptions != null)? taskOptions : $('#TaskOpt').html();
    projOptions = (projOptions != "" && projOptions != null) ? projOptions : $('#ProjectOpt').html();
    var html = '<BR /><BR /><BR /><BR /><ul id="toggle-view">';
    var subHeaders = ["Customer","Project","Task","Description","Hours"];
    var subOptions = [custOptions,projOptions,taskOptions];

    for(var item in itemsArray)
    {
        var selectTd = "";
        html += '<li><h3>Timesheet Details - '+itemsArray[item][6]+'</h3><span class="toggleSpan">+</span><div class="panel">';
        html += '<TABLE cellspacing="5" align="center" class="longer"><THEAD>';
        html += '<TR><TD colspan="8" rowspan="1" class="header">Timesheet Details - '+itemsArray[item][6]+'</TD></TR><TR>';
        $.each(subHeaders, function( i, vals ){
            var noteIcon = (i==3) ? '' : '<SPAN class="note">*</SPAN>';
            html += '<TD colspan="1" rowspan="1" class="subheader">'+vals+noteIcon+'</TD>';
            if(i < 3)
                selectTd += '<TD><SELECT tabindex="1" name="'+vals+(parseInt(item)+1)+'" class="long_ seltest" id="'+vals+(parseInt(item)+1)+'">'+subOptions[i]+'</OPTION></SELECT></TD>';
        });
        html += '</TR></THEAD><TBODY><TR>'+selectTd;
        html += '<TD colspan="1" rowspan="1" class="center"><INPUT tabindex="1" name="Description'+(parseInt(item)+1)+'" type="text" class="standard" maxlength="100" value=""/></TD>';
        html += '<TD colspan="1" rowspan="1" class="center"><INPUT tabindex="1" name="Hours'+(parseInt(item)+1)+'" type="text" step="0.01" class="veryshort" maxlength="5" value=""/></TD>';
        html += '<td style="display:none;">'+(parseInt(item)+1)+'</td><TD colspan="'+(parseInt(item)+1)+'"><INPUT tabindex="1" name="Submit'+(parseInt(item)+1)+'" type="button" class="button button_" value="Add"/></TD></TR>';
        html += '</TBODY></TABLE></div></li>';
    }
    html += '</ul>';
    return html;
}
function BuildHtmlDSA(itemsArrays){
    activityOptions = (activityOptions != "" && activityOptions != null) ? activityOptions : $('.activity1Opt').html();
    completionOptions = (completionOptions != "" && completionOptions != null)? completionOptions : $('select[name="completion1Opt"]').html();
    var html = '<BR /><BR /><BR /><BR /><ul id="toggle-view">';
    var subHeaders = ["Project","WPos","Activity","Description","Comment","Hours","Completion"];
    var subOptions = [activityOptions,completionOptions];

    for(var item=0; item < itemsArrays.length; item++)
    {
       
        html += '<li><h3>Timesheet Details - '+itemsArrays[item][6]+'</h3><span class="toggleSpan">+</span><div class="panel">';
        html += '<TABLE cellspacing="5" align="center" class="longer"><THEAD>';
        html += '<TR><TD colspan="9" rowspan="1" class="header">Timesheet Details - '+itemsArrays[item][6]+'</TD></TR><TR>';
        $.each(subHeaders, function( i, vals ){
            html += '<TD colspan="1" rowspan="1" class="subheader">'+vals+'<SPAN class="note">*</SPAN></TD>';
        });
        html += '</TR></THEAD><TBODY><TR>'+
                '<TD colspan="1" rowspan="1" class="center"><INPUT tabindex="1" onkeyup="'+scheduleAutoComplete(this,(parseInt(item)+1),'')+'" name="Project'+(parseInt(item)+1)+'" type="text" class="veryshort" maxlength="8" value=""/></TD>'+
                '<TD colspan="1" rowspan="1" class="center"><INPUT tabindex="1" onkeyup="'+scheduleAutoComplete(this,(parseInt(item)+1),'')+'" name="WPos'+(parseInt(item)+1)+'" type="text" class="veryshort" maxlength="8" value=""/></TD>'+
                '<TD><SELECT tabindex="1" name="Activity'+(parseInt(item)+1)+'" class="standard seltest">'+activityOptions+'</OPTION></SELECT></TD>';
        html += '<TD colspan="1" rowspan="1" class="center"><INPUT tabindex="1" name="Description'+(parseInt(item)+1)+'" type="text" class="standard" maxlength="100" value=""/></TD>'+
                '<TD colspan="1" rowspan="1" class="center"><INPUT tabindex="1" name="Comment'+(parseInt(item)+1)+'" type="text" class="standard" maxlength="100" value=""/></TD>';
        html += '<TD colspan="1" rowspan="1" class="center"><INPUT tabindex="1" name="Hours'+(parseInt(item)+1)+'" type="text" step="0.01" class="veryshort" maxlength="5" value=""/></TD>'+
                '<TD><SELECT tabindex="1" name="Completion'+(parseInt(item)+1)+'" class="veryshort">'+completionOptions+'</OPTION></SELECT></TD>';
        html += '<td style="display:none;">'+(parseInt(item)+1)+'</td><TD colspan="'+(parseInt(item)+1)+'"><INPUT tabindex="1" name="Submit'+(parseInt(item)+1)+'" type="button" class="button dsaButton" value="Add"/></TD></TR>';
        html += '</TBODY></TABLE></div></li>';
    }
    html += '</ul>';
    return html;
}