################################################################################
#This shell script is used to create links to all Purchase Orders, Quotes and 
#RFQs so that they can be used by the intranet.
################################################################################
archiveDir="/home/e-smith/files/ibays/s4_admin/files"
currentDir="/home/e-smith/files/ibays/s4_current/files/Current"
intranetDir="/home/e-smith/files/ibays/intranew/html/Files/Links"
################################################################################
# Create links to all stored files.
################################################################################
#All archived files.
echo "Linking archived files..."
Y=$(date +%Y)
for ((i=2004; $i<=($Y-1); i++)); do
  for j in ${archiveDir}/${i}/Current/POs/*; do
    if [ -f "${j}" ]; then
      echo "Linking $j"
      ln -f "${j}" "${intranetDir}/POs/"
    fi
  done
  for j in ${archiveDir}/${i}/Current/Quotes/*; do
    if [ -f "${j}" ]; then
      echo "Linking $j"
       ln -f "${j}" "${intranetDir}/Quotes/"
    fi
  done
  for j in ${archiveDir}/${i}/Current/RFQs/*; do
    if [ -f "${j}" ]; then
      echo "Linking $j"
      ln -f "${j}" "${intranetDir}/RFQs/"
    fi
  done
done
#All current files.
echo "Linking current files..."
for i in $currentDir/POs/*; do
  if [ -f "${i}" ]; then
    echo "Linking $i"
    ln -f "${i}" "${intranetDir}/POs/"
  fi
done
for i in $currentDir/Quotes/*; do
  if [ -f "${i}" ]; then
    echo "Linking $i"
    ln -f "${i}" "${intranetDir}/Quotes/"
  fi
done
for i in $currentDir/RFQs/*; do
  if [ -f "${i}" ]; then
    echo "Linking $i"
    ln -f "${i}" "${intranetDir}/RFQs/"
  fi
done