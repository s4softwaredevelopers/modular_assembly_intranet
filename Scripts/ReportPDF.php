<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require('fpdf/WriteHTML.php');

function CreatePdfTable($pdf,$columnLabels,$data,$row,$loCurrencySymbol)
{
    // Begin configuration
    $textColour = array( 0, 0, 0 );
    $tableHeaderTopTextColour = array( 255, 255, 255 );
    $tableHeaderTopFillColour = array( 20, 70, 120 );
    $tableBorderColour = array( 50, 50, 50 );
    $tableRowFillColour = array( 213, 170, 170 );
    $columnSize = array(70,30,15,30,15,30);
    // End configuration

    //Create the table
    $pdf->SetDrawColor( $tableBorderColour[0], $tableBorderColour[1], $tableBorderColour[2] );
    $pdf->Ln(3);

    // Create the table header row
    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->SetTextColor( $tableHeaderTopTextColour[0], $tableHeaderTopTextColour[1], $tableHeaderTopTextColour[2] );
    $pdf->SetFillColor( $tableHeaderTopFillColour[0], $tableHeaderTopFillColour[1], $tableHeaderTopFillColour[2] );
    for ( $i=0; $i<count($columnLabels); $i++ ) {
        if($i==0)
            $pdf->Cell( $columnSize[$i], 8, $columnLabels[$i], 0, 0, 'L', true );
        else
            $pdf->Cell( $columnSize[$i], 8, $columnLabels[$i], 0, 0, 'C', true );
    }
    $pdf->Ln();

    // Create the table data rows
    foreach ( $data as $dataRow ) {

        // Create the data cells
        $pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );
        $pdf->SetFillColor( $tableRowFillColour[0], $tableRowFillColour[1], $tableRowFillColour[2] );
        $pdf->SetFont( 'Arial', '', 8 );
        $pdf->SetAligns(array('L','L','R','R','R','R'));
        $pdf->SetWidths(array(70,30,15,30,15,30));
        $pdf->Row(array($dataRow[0], $dataRow[1], $dataRow[2],$dataRow[3],$dataRow[4],$dataRow[5]));
    }

    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->SetFillColor( $tableHeaderTopFillColour[0], $tableHeaderTopFillColour[1], $tableHeaderTopFillColour[2] );
    for ( $i=0; $i<count($columnLabels); $i++ ) {
        if(($i+1) == count($columnLabels))
        {
            $pdf->SetTextColor( $tableHeaderTopTextColour[0], $tableHeaderTopTextColour[1], $tableHeaderTopTextColour[2] );
            $pdf->Cell( $columnSize[$i], 10, $loCurrencySymbol.$row['OrderNo_Original_Total_Cost'], 0, 0, 'R', true );
        }
        else
            if(($i+2) == count($columnLabels))
                $pdf->Cell( $columnSize[$i]-4, 10, "Total");
            else
                $pdf->Cell( $columnSize[$i]+1, 10, "", 0, 0, 'C', false );
    }
    $pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );
}

function BuildPdfContent($pdf,$contentLabelArray,$contentValueArray)
{
    for ( $i=0; $i<count($contentLabelArray); $i++ ) {
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(30, 5, $contentLabelArray[$i], 0, 0, 'L');
        $pdf->SetFont('Arial','',8);
        if($i==4)
        {
            $cellWidth = $pdf->GetStringWidth($contentValueArray[$i]);
            if(($contentValueArray[$i] == 'Pending') || ($contentValueArray[$i] == 'Yes'))
            {
                if($contentValueArray[$i] == 'Pending')
                    $pdf->SetFillColor('255','0','0');
                else
                    if($contentValueArray[$i] == 'Yes')
                        $pdf->SetFillColor('0','255','0');

                $pdf->Cell($cellWidth + 3, 5, $contentValueArray[$i], 0, 0, 'L',true);
            }
            else
                $pdf->Cell(0, 5, $contentValueArray[$i], 0, 0, 'L');
        }
        else if($i==7)
        {
            if(strlen($contentValueArray[$i]) < 121)
                $pdf->Cell(0, 5, $contentValueArray[$i], 0, 0, 'L');
            else
                $pdf->MultiCell( 150, 5, $contentValueArray[$i], 0);
        }
        else
            $pdf->Cell(0, 5, $contentValueArray[$i], 0, 0, 'L');

        $pdf->SetFillColor(213, 170, 170);
        $pdf->Ln();
    }
}

function GeneratePdf($pdf_filename,$columnLabels,$tableData,$resultSet,$staffRow,$loCurrencySymbol)
{

    $pdf=new PDF_HTML();
    //Create Page
    $pdf->AliasNbPages();
    $pdf->SetAutoPageBreak(true, 36);
    $pdf->AddPage();

    while ($row = MySQL_Fetch_Array($resultSet))
    {
        $pdf->SetFont('Arial','B',10);
        $pdf->WriteHTML('<P><B><U>Internal Order:</U></B></P>');
        $pdf->Ln(5);
        $contentLabelArray = array('Date :','Order No :','Project:','Supplier:','Approved:','Approved By:','Total(Excl.VAT) :','Requested By:','Comment:');
        switch ($row['OrderNo_Approved'])
        {
            case '0':
                $isApproved = 'Pending';
                break;
            case '1':
                $isApproved = 'Yes';
                break;
            case '2':
                $isApproved = 'Declined';
                break;
            default:
                break;
        }

        $ProjectRow = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Project WHERE Project_Code = "'.$row['OrderNo_Project'].'"'));
        $SupplierRow = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Supplier WHERE Supplier_Code = "'.$row['OrderNo_Supplier'].'"'));
        $RequestorRow = MySQL_Fetch_Array(ExecuteQuery('SELECT CONCAT(Staff_First_Name, " ", Staff_Last_Name) AS RequestorName FROM Staff WHERE Staff_Code = '.$row['OrderNo_Requested'].''));

        //Get Currency
        $loCurrencyRow = MySQL_Fetch_Array(ExecuteQuery('SELECT C.Currency_Rate, C.Currency_Symbol FROM Currency C WHERE Currency_ID = "'.$row['OrderNo_Original_Currency'].'"'));
        $loCurrencySymbol = $loCurrencyRow['Currency_Symbol'];

        $contentValueArray = array(GetTextualDateFromDatabaseDate($row['OrderNo_Date_Time']),$row['OrderNo_Number'],$ProjectRow['Project_Pastel_Prefix'].' - '.$ProjectRow['Project_Description'],$SupplierRow['Supplier_Name'].' - '.$SupplierRow['Supplier_Phone'],$isApproved,$row['ApprovedBy'],$loCurrencySymbol.$row['OrderNo_Original_Total_Cost'],$RequestorRow['RequestorName'],$row['OrderNo_Comments']);
        BuildPdfContent($pdf,$contentLabelArray,$contentValueArray);

        //Create the table
        $itemsResultSet = ExecuteQuery('SELECT * FROM Items WHERE Items_Order_Number = "'.$row['OrderNo_Number'].'"');
        while ($rowC = MySQL_Fetch_Array($itemsResultSet))
        {
            $tableData[] = array($rowC['Items_Description'],$rowC['Items_Supplier_Code'],$rowC['Items_Quantity'],$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Value']),$rowC['Items_Discount'].'%',$loCurrencySymbol.SPrintF('%02.2f', $rowC['Items_Original_Total']));
        }

        CreatePdfTable($pdf,$columnLabels,$tableData,$row,$loCurrencySymbol);
        unset($tableData);
        $pdf->Ln(5);

    }




    return $pdf;
}
function GenerateChatPdf($pdf_filename,$imageurl,$imageurl2,$stringdaterange)
{

    $pdf=new PDF_HTML();
    //Create Page
    $pdf->AliasNbPages();
    $pdf->SetAutoPageBreak(true, 36);
    $pdf->AddPage();


    $pdf->SetFont('Arial','B',10);
    $pdf->SetTextColor(11, 49, 98 );
    $pdf->WriteHTML('<P><B>'.$stringdaterange.'</B></P>');
    $pdf->Ln();
    $pdf->WriteHTML('<HR>');
    $pdf->Ln(5);
    $pdf->Image($imageurl,10,50,190,0,'PNG');
    $pdf->Ln(5);
    $pdf->Image($imageurl2,10,140,190,0,'PNG');
    return $pdf;
}

function GenerateTimesheetPdf($pdf_filename,$tableData)
{
    $pdf=new PDF_HTML();
    //Create Page
    $pdf->AliasNbPages();
    $pdf->SetAutoPageBreak(true, 36);
    $pdf->AddPage();



    $NoOfDays = strtotime($_SESSION['GenerateTimesheetOverview'][3]) - strtotime($_SESSION['GenerateTimesheetOverview'][2]);
    $NoOfDays = $NoOfDays/(60*60*24);
    $end = $_SESSION['GenerateTimesheetOverview'][1];
    $endDate = GetDatabaseDateFromSessionDate($end);
    $loQueryResult = ExecuteQuery("SELECT CONCAT(st.Staff_First_Name, ' ', st.Staff_Last_Name) AS staffname, st.Staff_Code AS staffid FROM Staff st,ts_accesscontrol ts WHERE st.Staff_Code = ts.staffid AND ts.ts_access >0 AND st.Staff_IsEmployee > '0' ORDER BY staffname");

    if (MySQL_Num_Rows($loQueryResult) > 0)
    {

        //$loEmployees = privModelGetIndexedColumn($tableData, "ts_working_hours_ID", "ts_working_hours_Name");
        //$loIndexed = $loEmployees;
        $loDate = $endDate." 23:59:59";
        $loDOWs = array();
        for ($i = $NoOfDays; $i >=0; $i--)
        {
            $loDOWs[date('D', strtotime($loDate. " - $i days"))] = date('Y-m-d 00:00:00', strtotime($loDate. " - $i days"));
        }
        $loTextDays = array_keys($loDOWs);

        while ($newrow = MySQL_Fetch_Array($loQueryResult))
        {
            //echo "co0ollll";
            $loWeekout = array();
            $loProject =  array();
            $loCustomer = array();
            foreach ($loDOWs as $key => $day)
            { $isHoliday ='';
                $loTempQ =  "SELECT 'staffhours' AS staffhours, IFNULL(SUM(hours), 0) AS hours,project,customer,user FROM ts_entries WHERE datetime='$day' AND user='".$newrow['staffid']."'";
                //echo $loTempQ;
                $loSumResult = ExecuteQuery($loTempQ);
                $loSumTable = MySQL_Fetch_Array($loSumResult);
                $isHoliday = date('D', strtotime($day));
                $loMultiplier = 1;
                if ($isHoliday == "Sun")
                    $loMultiplier = 2;
                if ($isHoliday == "Sat")
                    $loMultiplier = 1.5;
                $loTotalHours = ($isHoliday == "Sat") || ($isHoliday == "Sun") ? $loSumTable["hours"] * $loMultiplier : $loSumTable["hours"] + (max($loSumTable["hours"] - 8, 0))*1.5;
                $extraHours = $loSumTable["hours"] - 8;
                if(($extraHours > 0) && (($isHoliday != "Sat") && ($isHoliday != "Sun")))
                {
                    $loTotalHours = $loTotalHours - $extraHours;
                }

                $projectR = MySQL_Fetch_Array(ExecuteQuery("SELECT Project_Pastel_Prefix,Project_Description FROM `Project` WHERE Project_Code ='".$loSumTable["project"]."'"));
                $CustR = MySQL_Fetch_Array(ExecuteQuery("SELECT Customer_Name FROM `Customer` WHERE Customer_Code ='".$loSumTable["customer"]."'"));
                $loWeekout[] = array($loTotalHours,$CustR["Customer_Name"],$projectR["Project_Description"],$projectR["Project_Pastel_Prefix"]);
                //$loWeekout[] = $loSumTable["project"];
                //$loWeekout[] = $loSumTable["customer"];
            }

            $newarray = array();
            for ($i=0; $i < sizeof($loWeekout); $i++)
            {
                $newarray[$loTextDays[$i]] = $loWeekout[$i];
            }
            $loOutputTable[$newrow['staffname']] = $newarray;
        }
        //print_r($loOutputTable);
        //echo "==============================";
    }

    /**while ($row = MySQL_Fetch_Array($resultSet))
    {
    $pdf->SetFont('Arial','B',10);
    $pdf->WriteHTML('<P><B><U>Timesheet:</U></B></P>');
    $pdf->Ln(5);
    $contentLabelArray = array('Staff Name :','Order No :');

    $StaffRow = MySQL_Fetch_Array(ExecuteQuery('SELECT CONCAT(Staff_First_Name," ",Staff_Last_Name) As Name WHERE Staff_Code = "'.$row['ts_working_hours_Name'].'"'));


    $contentValueArray = array($StaffRow['Name']);
    BuildPdfContent($pdf,$contentLabelArray,$contentValueArray);

    //Project


    //Create the table
    $itemsResultSet = ExecuteQuery('SELECT * FROM ts_entries WHERE ts_entries_ParentID = "'.$row['ts_working_hours_ID'].'"');

    while ($rowC = MySQL_Fetch_Array($itemsResultSet))
    {
    $ProjectRow = MySQL_Fetch_Array(ExecuteQuery('SELECT CONCAT(Project_Pastel_Prefix," - ",Project_Description) AS ts_project FROM Project WHERE Project_Code = "'.$rowC['project'].'"'));
    $CustRow = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Customer WHERE Customer_Code = "'.$rowC['customer'].'"'));
    $tableData[] = array($CustRow['Customer_Name'],$ProjectRow['ts_project'],$rowC['hours']);
    }



    }**/

    CreateTimesheetPdfTable($pdf,$loOutputTable);
    unset($loOutputTable);
    $pdf->Ln(5);

    unset($_SESSION['GenerateTimesheetOverview']);
    return $pdf;
}
function CreateTimesheetPdfTable($pdf,$data)
{
    //print_r($data);
    $countrows =0;
    //foreach ($data as $key => $value)//Headers
    //$countrows++;

    // $equalWidth = (190/$countrows) - 6;


    //$countrows =0;
    foreach ($data as $key => $val)//Headers
    {

        BuildTImesheetPdfContent($pdf,array("Staff Name: "),array($key));
        $pdf->Ln(3);

        TmesheetPdfConfig($pdf,"H");
        $tRows = array();
        $t_align = array();
        $t_width = array();
        $pdf->Cell( 60, 8, "CUSTOMER", 0, 0, 'L', true );
        $pdf->Cell( 60, 8, "PROJECT", 0, 0, 'L', true );
        $pdf->Cell( 15, 8, "CODE", 0, 0, 'L', true );
        $i =0;
        $totalhours =0;
        foreach ($data[$key] as $value => $test)//Headers
        {
            $pdf->Cell( 10, 8, strtoupper($value), 0, 0, 'L', true );
            $colsCount=0;
            //if($i == 0) $tRows[] = $data[$key][$value][1];
            //$tRows[] = $data[$key][$value][0];
            if($i == 0) {$t_align[] = 'L';$t_align[] = 'L';$t_align[] = 'L';}
            $t_align[] = 'L';
            if($i == 0) {$t_width[] =60; $t_width[] =60; $t_width[] =15;}
            $t_width[] = 10;
            $irow = array();
            for($y=0; $y< sizeof($data[$key]); $y++)
            {
                if($y == 0){ $irow[] = $data[$key][$value][1]; $irow[] = $data[$key][$value][2];$irow[] = $data[$key][$value][3];}
                if($y == $i){ $irow[] = $data[$key][$value][0]; $totalhours = $totalhours + floatval($data[$key][$value][0]);}
                else $irow[] = " ";
            }
            $tRows[] = $irow;
            $i++;
        }

        $pdf->Ln();
        TmesheetPdfConfig($pdf,"");
        $pdf->SetAligns($t_align);
        $pdf->SetWidths($t_width);
        foreach ($tRows as $myval)
        {
            $pdf->Row($myval);
        }

        //echo  "<BR /><BR />";
        //$countrows++;
        $pdf->SetFont( 'Arial', 'B', 8 );
        $pdf->SetFillColor( 20, 70, 120 );
        $cwidth = 10;
        for ( $c=0; $c<($i+3); $c++ ) {
            if($c ==2) $cwidth =15;
            else if($c ==0 || $c ==1) $cwidth =60;
            else $cwidth = 10;

            if(($c+2) == ($i+3))
            {
                $pdf->SetTextColor( 255, 255, 255 );
                $pdf->Cell( ($cwidth*2), 8, $totalhours, 0, 0, 'R', true );
            }
            else
                if(($c+3) == ($i+3))
                    $pdf->Cell( $cwidth, 8, "Total");
                else
                    $pdf->Cell( $cwidth, 8, "", 0, 0, 'C', false );
        }
        $pdf->SetTextColor( 0, 0, 0);
        $pdf->Ln(10);
    }

    /**foreach ($data[0] as $key => $value)//Headers
    {
    if($countrows == 0)
    $pdf->Cell( ($equalWidth+12), 8, strtoupper($key), 0, 0, 'L', true );
    else
    $pdf->Cell( $equalWidth, 8, strtoupper($key), 0, 0, 'L', true );

    $countrows++;
    }

    $pdf->Ln();**/

    // Create the table data rows
    /**foreach ($data as $key => $value) //each row
    {
    // Create the data cells
    $pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );
    $pdf->SetFillColor( 255, 255, 255 );
    $pdf->SetFont( 'Arial', '', 8 );
    //$pdf->SetAligns(array('L','L','R','R','R','R'));
    //$pdf->SetWidths(array(70,30,15,30,15,30));

    $loSum = 0;
    $dataRow = array();
    //$t_width =  array();
    //$t_align = array();
    $countrows =0;
    foreach ($value as $key2 => $val2)
    {
    //if($countrows == 0) $pdf->Cell( ($equalWidth+12), 8, $val2, 0, 0, 'L', true );
    //else $pdf->Cell( ($equalWidth), 8, $val2, 0, 0, 'C', true );
    $dataRow[] = $val2;
    //if($countrows == 0) $t_width[] = ($equalWidth+12);
    //else $t_width[] = $equalWidth;
    $t_align[] = 'L';
    if($countrows == 0) $t_width[] = ($equalWidth+12);
    else $t_width[] = $equalWidth;

    $countrows++;
    }
    //$pdf->Ln();
    $pdf->SetAligns($t_align);
    $pdf->SetWidths($t_width);
    $pdf->Row($dataRow);**

    }*/
}
function BuildTImesheetPdfContent($pdf,$contentLabelArray,$contentValueArray)
{
    for ( $i=0; $i<count($contentLabelArray); $i++ ) {
        $pdf->SetFont('Arial','B',8);
        $pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );
        $pdf->SetFillColor(213, 170, 170);
        $pdf->Cell(30, 5, $contentLabelArray[$i], 0, 0, 'L');
        $pdf->Cell(0, 5, $contentValueArray[$i], 0, 0, 'L');
        $pdf->Ln();
    }
}
function TmesheetPdfConfig($pdf,$position)
{

    // Begin configuration
    $textColour = array( 0, 0, 0 );
    $tableHeaderTopTextColour = array( 255, 255, 255 );
    $tableHeaderTopFillColour = array( 20, 70, 120 );
    $tableBorderColour = array( 50, 50, 50 );
    $tableRowFillColour = array( 213, 170, 170 );
    // End configuration

    if($position == "H")
    {
        //Create the table
        $pdf->SetDrawColor( $tableBorderColour[0], $tableBorderColour[1], $tableBorderColour[2] );
        $pdf->Ln(3);

        // Create the table header row
        $pdf->SetFont( 'Arial', 'B', 8 );
        $pdf->SetTextColor( $tableHeaderTopTextColour[0], $tableHeaderTopTextColour[1], $tableHeaderTopTextColour[2] );
        $pdf->SetFillColor( $tableHeaderTopFillColour[0], $tableHeaderTopFillColour[1], $tableHeaderTopFillColour[2] );
    }
    else
    {
        // Create the data cells
        $pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );
        $pdf->SetFillColor( 255, 255, 255 );
        $pdf->SetFont( 'Arial', '', 8 );
    }
}




?>