<?php
function AuthExceptions($paPage, $paUser)
{
    $allowed = false;
    $exceptions["Quotations.php"] = array(215, 267,306,308,402,412,489);
    $exceptions["DeliveryNotes.php"] = array(215);
    $exceptions["PriceLists.php"] = array(215);
    $exceptions["Leave.php"] = array(255);
    $exceptions["Invoices.php"] = array(321,303,441,499);
    $exceptions["Message.php"] = array(15,318,392,413,410);
    $exceptions["TimesheetProjects"] = array(032,488); //032-> Cherie, 488->Renaldo
    foreach ($exceptions as $key => $val)
    {
        foreach ($val as $num)
            if (($paPage == $key) && ($num == $paUser))
                $allowed = true;
    }
    return $allowed;
}

function FeatureExceptions($Page,$feature,$s4_staff)
{
    $amelia = '212';
    $cherié ='32';
    $roline='282';
    $andrew='15';
    $allowed =false;

    if(($Page == "PurchaseOrders") && ($feature=="Edit"))
    {
        if(($s4_staff == $cherié) || ($s4_staff==$andrew) || ($s4_staff==$amelia) || ($s4_staff==$roline))
        {
            $allowed=true;
        }
    }
    else
        if($Page == "Customers")
        {
            if(($s4_staff == $cherié) || ($s4_staff==$roline) || ($s4_staff==$andrew) || ($s4_staff==$amelia))
                $allowed=true;
        }
    return $allowed;
}

?>