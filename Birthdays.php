<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 25 February 2009                            //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page allows users to view staff birthdays.                          //
  //////////////////////////////////////////////////////////////////////////////
  
  include 'Scripts/Include.php';
  SetSettings();
  CheckAuthorisation('Birthdays.php');
  
  //////////////////////////////////////////////////////////////////////////////
?>  
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
  <HEAD>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Birthdays');
    include('Scripts/header.php');
      //////////////////////////////////////////////////////////////////////////
    ?>
  </HEAD>
  <BODY>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildBanner();
      //////////////////////////////////////////////////////////////////////////
    ?>
    <DIV class="contentcontainer">
      <?php
        // PHP SCRIPT //////////////////////////////////////////////////////////
        BuildMenu('Main', 'Birthdays.php');
        ////////////////////////////////////////////////////////////////////////
      ?>
      <DIV class="content">
        <BR /><BR />
        <?php
          // PHP SCRIPT ////////////////////////////////////////////////////////
          BuildContentHeader('Birthdays', "", "", false);
          $resultSet = ExecuteQuery('SELECT Staff.*, SUBSTRING(Staff_ID_Number, 3, 2) AS Month, SUBSTRING(Staff_ID_Number, 5, 2) AS Day FROM Staff WHERE Staff_IsEmployee > "0" AND LENGTH(Staff_ID_Number) = 13 AND Staff_Code <> "049" AND Staff_Code <> "057" ORDER BY Month, Day, Staff_First_Name, Staff_Last_Name ASC');
          
          $key = "0000";
          $birthdays = array();
          $birthdays['8190'] = array('19 '.Date('F', MKTime(0, 0, 0, 8 + 1, 0, 0)), 'Michael Wells', 8);
          while ($rowStaff = MySQL_Fetch_Array($resultSet))
          {
            if (SubStr($key, 0, 4) == $rowStaff['Month'].$rowStaff['Day'])
              $key = $rowStaff['Month'].$rowStaff['Day'].(SubStr($key, 4, 1) + 1);
            else
              $key = $rowStaff['Month'].$rowStaff['Day'].'0';
            $birthdays[$key] = array($rowStaff['Day'].' '.Date('F', MKTime(0, 0, 0, $rowStaff['Month'] + 1, 0, 0)), $rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name'], $rowStaff['Month']);
          }
          KSort($birthdays);
          
          echo '<DIV class="contentflow">
                  <P>These are the staff birthdays. Birthdays highlighted separately from the rest are those for the current month.</P>
                  <BR /><BR />
                  <TABLE cellspacing="5" align="center" class="short">
                    <TR>
                      <TD colspan="2" class="header">Birthday Details
                      </TD>
                    </TR>
                    <TR>
                      <TD class="subheader">Staff Name
                      </TD>
                      <TD class="subheader veryshort">Birthday
                      </TD>
                    </TR>
                    <TR>';
                    $month = Date('m');
                    foreach ($birthdays as $birthday)
                    {
                      if ($month == $birthday[2])
                        $colour = 'rowB';
                      else
                        $colour = 'rowA'; 
                      
                      echo '<TR>
                              <TD class="'.$colour.'">'.$birthday[1].'
                              </TD>
                              <TD class="'.$colour.' center">'.$birthday[0].'
                              </TD>
                            </TR>';
                    }
            echo '</TABLE>
                </DIV>';
        ?>
        <BR /><BR />  
      </DIV>
    </DIV>
    <?php 
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildFooter();
      //////////////////////////////////////////////////////////////////////////
    ?>    
  </BODY>
</HTML>