<?php
include 'Scripts/Include.php';
SetSettings();
?>
<html>
<head>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Error');
      //////////////////////////////////////////////////////////////////////////
    ?>
    <META http-equiv="REFRESH" content="5;url=index.php">
</head>
<body>
    <?php
    // PHP SCRIPT ////////////////////////////////////////////////////////////
    BuildBanner();
    //////////////////////////////////////////////////////////////////////////
    ?>
    <DIV id="main">
        <?php
            // PHP SCRIPT //////////////////////////////////////////////////////////
            include('Scripts/header.php');
            BuildTopBar();
            BuildMenu('Main', 'Error.php');

            function GetColour($Group)
            {
                switch ($Group)
                {
                    case 5:
                        return 'sky';
                        break;
                    case 6:
                        return 'rowA';
                        break;
                    case 8:
                        return 'rowB';
                        break;
                    default;
                        return "";
                        break;
                }
            }
        ////////////////////////////////////////////////////////////////////////
        ?>

        <section id="content_wrapper">
            <?php BuildBreadCrumb();?>
            <!-- -------------- Content -------------- -->
            <section id="content" class="table-layout">
                <!-- -------------- Column Center -------------- -->
                <div class="chute chute-center" style="height: 869px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
                                <?php
                                    // PHP SCRIPT ////////////////////////////////////////////////////////
                                    BuildMessageSet('Home');
                                    //////////////////////////////////////////////////////////////////////
                                    ///
                                    CheckStatus();
                                    ///
                                    BuildContentHeader('Error', "", "", false);
                                    echo '<BR style="line-height:100px;" />
                                                <DIV class="contentflow">
                                                    There is a problem with the page you requested. Either it was not found or an error occurred while processing it.
                                                    <BR /><BR />
                                                    Please wait while you are redirected to the <A href="index.php">Home</A> page...
                                                </DIV>';
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </DIV>

    <?php
        // PHP SCRIPT ////////////////////////////////////////////////////////////
        BuildFooter();
        //////////////////////////////////////////////////////////////////////////
    ?>
</body>
</html>