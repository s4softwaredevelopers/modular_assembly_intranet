<?php

include 'Scripts/Include.php';
SetSettings();
CheckAuthorisation('Phonelist.php');

//////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
<HEAD>
    <?php
    // PHP SCRIPT ////////////////////////////////////////////////////////////
    BuildHead('Phonelist');
    include ('Scripts/header.php');
    //////////////////////////////////////////////////////////////////////////
    ?>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">

    <SCRIPT src="Scripts/FileScripts/Phonelist.js"></SCRIPT>
    <STYLE type="text/css">
        #gridContainer {
            height: auto;
            margin: auto;
            width: 80%;
        }
        .hidden{
            display: none;
        }

    </STYLE>
    <div class="button" id="showToastButton"></div><div id="myToast"></div>

</HEAD>
<BODY>
<?php
// PHP SCRIPT ////////////////////////////////////////////////////////////
BuildBanner();
//////////////////////////////////////////////////////////////////////////
?>
<DIV id="main">
    <?php
    BuildTopBar();
    // PHP SCRIPT //////////////////////////////////////////////////////////
    BuildMenu('Main', 'Phonelist.php');
    ////////////////////////////////////////////////////////////////////////
    ?>
    <section id="content_wrapper">
        $current
        <?php BuildBreadCrumb($currentPath);?>
        <!-- -------------- Content -------------- -->
        <section id="content" class="table-layout">
            <!-- -------------- Column Center -------------- -->
            <div class="chute chute-center" style="height: 869px;">

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
        <?php
        // PHP SCRIPT ////////////////////////////////////////////////////////
        BuildMessageSet('Phonelist');
        //////////////////////////////////////////////////////////////////////
        ?>
        Contact Kayla Schoeman to update contact details
        <br/>
        <br/>
        View telephone manual
        <a href="./Files/Site/Phone_Quick_Guide.pdf" target="_blank">here.</a>
        <div id ="gridContainer"></div>
   
                    </div>
                </div>
            </div>
        </section>
</DIV>
<?php
// PHP SCRIPT ////////////////////////////////////////////////////////////
BuildFooter();
//////////////////////////////////////////////////////////////////////////
?>

</BODY>
</HTML>
