<?php
include 'Scripts/Include.php';
SetSettings();
CheckAuthorisation('ReleaseNotes.php');
?>
<html>
<head>
    <?php
      // PHP SCRIPT ////////////////////////////////////////////////////////////
      BuildHead('Release Notes');
      //include('Scripts/header.php');
      //////////////////////////////////////////////////////////////////////////
    ?>
    <div class="button" id="showToastButton"></div><div id="myToast"></div>

    <meta http-equiv="content-type" content="text/html; charset=utf-8">
      <script src="Scripts/DevExpress/js/jquery-2.1.3.min.js" type="text/javascript"></script>
      <script src="Scripts/DevExpress/js/jszip.min.js" type="text/javascript"></script>
      <script src="Scripts/DevExpress/js/dx.custom_16.1.8.js" type="text/javascript"></script>
      <link href="Stylesheets/dx.common16.1.8.css" rel="stylesheet" type="text/css"/>
      <link href="Stylesheets/dx.light-compact.css" rel="stylesheet" type="text/css"/>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

      <link href="./Stylesheets/release_notes_theme.css" rel="stylesheet" type="text/css">
    
      <style>
            @import "https://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700";
            header:after, #timeline .timeline-item:after, header:before, #timeline .timeline-item:before {
              content: '';
              display: block;
              width: 100%;
              clear: both;
            }

            i {
                font-size: 20px;
            }

            .project-name {
              text-align: center;
              padding: 10px 0;
              color: #295885;
              font-weight: 100;
            }
            .group-btn{
               margin-top: 38px;
               margin-right: -10px;
            }
            .release-date{
                float: left;
                margin-top: 40px;
                margin-left: -6px;
                color: gray;
            }
            
            header .logo {
              color: #295885;
              float: left;
              font-family: "Dosis", arial, tahoma, verdana;
              font-size: 22px;
              font-weight: 500;
            }
            header .logo > span {
              color: #f7aaaa;
              font-weight: 300;
            }
            header .social {
              float: right;
            }
            header .social .btn {
              font-family: "Dosis";
              font-size: 14px;
              margin: 10px 5px;
            }
            #hide {
                overflow: hidden;
            }
            #timeline {
              width: 99%;
              margin: 30px auto;
              position: relative;
              padding: 0 10px;
              -webkit-transition: all 0.4s ease;
              -moz-transition: all 0.4s ease;
              -ms-transition: all 0.4s ease;
              transition: all 0.4s ease;
            }
            #timeline:before {
              content: "";
              width: 3px;
              height: 100%;
              background: #295885;
              left: 50%;
              top: 0;
              position: absolute;
            }
            #timeline:after {
              content: "";
              clear: both;
              display: table;
              width: 100%;
            }
            #timeline .timeline-item {
              margin-bottom: 50px;
              position: relative;
            }
            #timeline .timeline-item .timeline-icon {
              background: #295885;
              width: 50px;
              height: 50px;
              position: absolute;
              top: 0;
              left: 50%;
              overflow: hidden;
              margin-left: -23px;
              -webkit-border-radius: 50%;
              -moz-border-radius: 50%;
              -ms-border-radius: 50%;
              border-radius: 50%;
            }
            #timeline .timeline-item .timeline-icon svg {
              position: relative;
              top: 14px;
              left: 14px;
            }
            #timeline .timeline-item .timeline-content {
              width: 40%;
              background: #fff;
              padding: 20px;
              height: 200px;
              -webkit-box-shadow: 0 3px 0 rgba(0, 0, 0, 0.1);
              -moz-box-shadow: 0 3px 0 rgba(0, 0, 0, 0.1);
              -ms-box-shadow: 0 3px 0 rgba(0, 0, 0, 0.1);
              box-shadow: 0 3px 0 rgba(0, 0, 0, 0.1);
              -webkit-border-radius: 5px;
              -moz-border-radius: 5px;
              -ms-border-radius: 5px;
              border-radius: 5px;
              -webkit-transition: all 0.3s ease;
              -moz-transition: all 0.3s ease;
              -ms-transition: all 0.3s ease;
              transition: all 0.3s ease;
            }
            #timeline .timeline-item .timeline-content h2 {
              padding: 15px;
              background: #295885;
              color: #fff;
              margin: -20px -20px 0 -20px;
              font-weight: 300;
              -webkit-border-radius: 3px 3px 0 0;
              -moz-border-radius: 3px 3px 0 0;
              -ms-border-radius: 3px 3px 0 0;
              border-radius: 3px 3px 0 0;
            }
            #timeline .timeline-item .timeline-content:before {
              content: '';
              position: absolute;
              left: 45%;
              top: 20px;
              width: 0;
              height: 0;
              border-top: 7px solid transparent;
              border-bottom: 7px solid transparent;
              border-left: 7px solid #295885;
            }
            #timeline .timeline-item .timeline-content.right {
              float: right;
            }
            #timeline .timeline-item .timeline-content.right:before {
              content: '';
              right: 45%;
              left: inherit;
              border-left: 0;
              border-right: 7px solid #295885;
            }

            .btn {
              padding: 3px 3px;
              text-decoration: none;
              background: transparent;
              position: relative;
              border-radius: 5px;
              transition: background 0.3s ease;
              float: right;
              color: #295885;
            }
            .btn:hover {
              box-shadow: none;
              top: 2px;
              left: 2px;
              -webkit-box-shadow: 2px 2px 0 transparent;
              -moz-box-shadow: 2px 2px 0 transparent;
              -ms-box-shadow: 2px 2px 0 transparent;
              box-shadow: 2px 2px 0 transparent;
            }

            @media screen and (max-width: 768px) {
              #timeline {
                margin: 30px;
                padding: 0px;
                width: 90%;
              }
              #timeline:before {
                left: 0;
              }
              #timeline .timeline-item .timeline-content {
                width: 90%;
                float: right;
              }
              #timeline .timeline-item .timeline-content:before, #timeline .timeline-item .timeline-content.right:before {
                left: 10%;
                margin-left: -6px;
                border-left: 0;
                border-right: 7px solid #295885;
              }
              #timeline .timeline-item .timeline-icon {
                left: 0;
              }
            }
  

      </style>

</head>
<body>
    <?php
    // PHP SCRIPT ////////////////////////////////////////////////////////////
    BuildBanner();
    //////////////////////////////////////////////////////////////////////////
    ?>
    <DIV id="main">
        <?php
            // PHP SCRIPT //////////////////////////////////////////////////////////
            BuildTopBar();
            BuildMenu('Main', 'Passwords.php');

            function GetColour($Group)
            {
                switch ($Group)
                {
                    case 5:
                        return 'sky';
                        break;
                    case 6:
                        return 'rowA';
                        break;
                    case 8:
                        return 'rowB';
                        break;
                    default;
                        return "";
                        break;
                }
            }
        ////////////////////////////////////////////////////////////////////////
        ?>

        <section id="content_wrapper">
            <?php BuildBreadCrumb();?>
            <!-- -------------- Content -------------- -->
            <section id="content" class="table-layout">
                <!-- -------------- Column Center -------------- -->
                <div class="chute chute-center" style="height: 869px;">
                
                    <?php
                        // PHP SCRIPT ////////////////////////////////////////////////////////
                        BuildMessageSet('Phonelist');

                        CheckStatus();

                        BuildContentHeader('Error', "", "", false);
                    ?>

                    <!-- <div class="container">-->
                    <h1 class="project-name">Welcome to the Release Notes screen. <br>
                      Here you can see and read about all the latest features, updates and bug fixes for the Modular Assembly Intranet.</h1>
		                <div id="timeline">

                        <!--//////////////////////////////////////
                        ///////////Release Note Three//////////////
                        //////////////////////////////////////-->
                        <div class="timeline-item">
                            <div class="timeline-icon">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="21px" height="20px" viewBox="0 0 21 20" enable-background="new 0 0 21 20" xml:space="preserve">
                                    <path fill="#FFFFFF" d="M19.998,6.766l-5.759-0.544c-0.362-0.032-0.676-0.264-0.822-0.61l-2.064-4.999
                                          c-0.329-0.825-1.5-0.825-1.83,0L7.476,5.611c-0.132,0.346-0.462,0.578-0.824,0.61L0.894,6.766C0.035,6.848-0.312,7.921,0.333,8.499
                                          l4.338,3.811c0.279,0.246,0.395,0.609,0.314,0.975l-1.304,5.345c-0.199,0.842,0.708,1.534,1.468,1.089l4.801-2.822
                                          c0.313-0.181,0.695-0.181,1.006,0l4.803,2.822c0.759,0.445,1.666-0.23,1.468-1.089l-1.288-5.345
                                          c-0.081-0.365,0.035-0.729,0.313-0.975l4.34-3.811C21.219,7.921,20.855,6.848,19.998,6.766z"/>
                                </svg>
                            </div>
                            
                            <div class="timeline-content">
                                <h2>Version 19-19 : New Features</h2>
                               <p>
                                  <ul>
                                      <li>Employee Vehicle Registration screen added</li>
                                      <li>Release Log Timeline added</li>
                                  </ul>  
                                </p>
                                <div class="release-date">09 May 2019</div>
                                <div class="group-btn"> 
                                    <a style="font-size:24px;background-color:transparent" href="Release_Notes/2019-05-16.pdf" download="V19-16"><i class="fa fa-download btn"></i></a>
                                    <a style="font-size:24px;background-color:transparent" href="Release_Notes/2019-05-16.pdf" target="_blank"><i class="fa fa-eye btn"></i></a>
                                </div>
                            </div>

                        </div>
                              
                              
                        <!--//////////////////////////////////////
                        ///////////Release Note Two//////////////
                        //////////////////////////////////////-->
                        <div class="timeline-item">
                            <div class="timeline-icon">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                    <g>
                                        <path fill="#FFFFFF" d="M7.074 1.408c0-.778.641-1.408 1.431-1.408.942 0 1.626.883 1.38 1.776-.093.336-.042.695.138.995.401.664 1.084 1.073 1.977 1.078.88-.004 1.572-.408 1.977-1.078.181-.299.231-.658.138-.995-.246-.892.436-1.776 1.38-1.776.79 0 1.431.63 1.431 1.408 0 .675-.482 1.234-1.118 1.375-.322.071-.6.269-.769.548-.613 1.017.193 1.917.93 2.823-1.21.562-2.524.846-3.969.846-1.468 0-2.771-.277-3.975-.84.748-.92 1.555-1.803.935-2.83-.168-.279-.446-.477-.768-.548-.636-.14-1.118-.699-1.118-1.374zm16.926 18.092c0 2.485-2.015 4.5-4.5 4.5s-4.5-2.015-4.5-4.5 2.015-4.5 4.5-4.5 4.5 2.015 4.5 4.5zm-2.156-.882l-.696-.696-2.116 2.169-.991-.94-.696.697 1.688 1.637 2.811-2.867zm-8.483 3c-.42.159-.864.293-1.361.366-6.151-.893-6.195-8.934-4.872-11.866 1.517.586 3.148.882 4.872.882 1.708 0 3.335-.298 4.859-.887.357.809.607 1.943.681 3.191.619-.197 1.277-.304 1.96-.304l.171.009c-.178-.175-.315-.398-.354-.683-.092-.677.286-1.147.765-1.333l2.231-.866c.541-.21.807-.813.594-1.346-.164-.408-.561-.657-.98-.657-.496 0-2.568 1.016-2.958 1.016-.255 0-.509-.135-.648-.454-.185-.423-.396-.816-.62-1.188-1.715.992-3.621 1.502-5.701 1.502-2.113 0-3.995-.498-5.703-1.496-.217.359-.421.738-.601 1.146-.161.366-.421.491-.663.491-.241 0-2.341-.854-2.573-.944-.543-.212-1.154.054-1.367.584-.213.533.053 1.136.594 1.346l2.231.866c.496.192.854.694.773 1.274-.106.758-.683 1.111-1.235 1.111h-2.402c-.582 0-1.054.464-1.054 1.037s.472 1.037 1.054 1.037h2.387c.573 0 1.159.372 1.265 1.057.112.728-.228 1.229-.751 1.462l-2.42 1.078c-.53.236-.766.851-.526 1.373.239.52.86.754 1.395.518l2.561-1.14c.336-.15.701-.083.901.259 1.043 1.795 3.143 3.608 6.134 3.941.843-.094 1.606-.315 2.304-.611-.4-.534-.72-1.129-.943-1.771z"/>
                                    </g>
                                </svg>
                            </div>

                            <div class="timeline-content right">
                                <h2>Version 19-20 : Bug Fixes</h2>
                                <p>
                                    <ul>
                                        <li>Bug 1331: Adding a Project</li>
                                        <li>Bug 1332: Editing a Project</li>
                                        <li>Bug 1333: Editing a Project with a Transport Category</li>
                                    </ul> 
                                </p>
                                <div class="release-date" style="margin-top:21px;">13 May 2019</div>
                            </div>
                        </div>
                              
                        <!--//////////////////////////////////////
                        ///////////Release Note One//////////////
                        //////////////////////////////////////-->
                        <div class="timeline-item">
                            <div class="timeline-icon">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="21px" height="20px" viewBox="0 0 21 20" enable-background="new 0 0 21 20" xml:space="preserve">
                                    <path fill="#FFFFFF" d="M19.998,6.766l-5.759-0.544c-0.362-0.032-0.676-0.264-0.822-0.61l-2.064-4.999
                                          c-0.329-0.825-1.5-0.825-1.83,0L7.476,5.611c-0.132,0.346-0.462,0.578-0.824,0.61L0.894,6.766C0.035,6.848-0.312,7.921,0.333,8.499
                                          l4.338,3.811c0.279,0.246,0.395,0.609,0.314,0.975l-1.304,5.345c-0.199,0.842,0.708,1.534,1.468,1.089l4.801-2.822
                                          c0.313-0.181,0.695-0.181,1.006,0l4.803,2.822c0.759,0.445,1.666-0.23,1.468-1.089l-1.288-5.345
                                          c-0.081-0.365,0.035-0.729,0.313-0.975l4.34-3.811C21.219,7.921,20.855,6.848,19.998,6.766z"/>
                                </svg>
                            </div>

                            <div class="timeline-content">
                                <h2>Version 19-20 : New Features</h2>
                                <p>
                                    <ul>
                                        <li>HR Expense Report Updates</li>
                                    </ul>  
                                </p>
                                <div class="release-date" style="margin-top:66px;">16 May 2019</div>
                                <div class="group-btn">

                                </div>
                            </div>
                        </div>

                        <!--//////////////////////////////////////
                        ///////////Start Icon Box//////////////
                        //////////////////////////////////////-->
                        <div class="timeline-item">
                            <div class="timeline-icon">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                    <g>
                                        <path fill="#FFFFFF" d="M17.677 16.879l-.343.195v-1.717l.343-.195v1.717zm2.823-3.324l-.342.195v1.717l.342-.196v-1.716zm3.5-7.602v11.507l-9.75 5.54-10.25-4.989v-11.507l9.767-5.504 10.233 4.953zm-11.846-1.757l7.022 3.2 1.7-.917-7.113-3.193-1.609.91zm.846 7.703l-7-3.24v8.19l7 3.148v-8.098zm3.021-2.809l-6.818-3.24-2.045 1.168 6.859 3.161 2.004-1.089zm5.979-.943l-2 1.078v2.786l-3 1.688v-2.856l-2 1.078v8.362l7-3.985v-8.151zm-4.907 7.348l-.349.199v1.713l.349-.195v-1.717zm1.405-.8l-.344.196v1.717l.344-.196v-1.717zm.574-.327l-.343.195v1.717l.343-.195v-1.717zm.584-.332l-.35.199v1.717l.35-.199v-1.717zm-16.656-4.036h-2v1h2v-1zm0 2h-3v1h3v-1zm0 2h-2v1h2v-1z"/>
                                    </g>
                                </svg>
                            </div>
                        </div>

                    </div>
                    
                </div>
            </section>
        </section>
    </DIV>

    <?php
        // PHP SCRIPT ////////////////////////////////////////////////////////////
        BuildFooter();
        //////////////////////////////////////////////////////////////////////////
    ?>
</body>
</html>