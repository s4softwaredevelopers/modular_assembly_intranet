<?php
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 15 July 2009                                //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  // This page handles the back-end for the Projects page.                    //
  //////////////////////////////////////////////////////////////////////////////
  

  include '../Scripts/Include.php';
  SetSettings();
  CheckLoggedIn();
  $_POST = Replace('"', '\'\'', $_POST);
  

  if(isset($_GET['function'])){
   switch ($_GET['function']){
       case 'getProjectMappingCode':
            getProjectMappingCode() ;  // make the function
            break;
        default:
            break;
     }
  
  }
  else{
  switch ($_POST['Type'])
  {
    //User has submitted information for a new project.
    case 'Add':
      HandleAdd();
    	break;
    //User has submitted modified information for a project.
    case 'Edit':
      HandleEdit();
    	break;
    //User has selected to Add or Edit a project.
    case 'Maintain':
      HandleMaintain();
      break;
    //User has submitted information for project history.
    case 'Update':
      HandleUpdate();
    	break;
      case 'Mapping':
          HandleMapping();
          break;
    //User has selected to view a project.
    case 'View':
    case 'ViewSingle':
      case 'ViewMapping':
      HandleView();
    	break;
    case 'CostManagers':
        HandleCostManager();
    //User has reached this page incorrectly. If they are not authorised they are redirected to the main page from the Projects page.
    default:
    	break;
  }
   Header('Location: ../Projects.php?'.Rand());
  }
  //////////////////////////////////////////////////////////////////////////////
  // Checks that all the required fields have values and that these values    //
  // are valid.                                                               //
  //////////////////////////////////////////////////////////////////////////////
  function CheckFields()
  {
      
    switch ($_POST['Type'])
    {
      case 'Edit':         
        if (($_POST['Orders'] == "") || ($_POST['WorkInProgress'] == "") || ($_POST['Complete'] == ""))
          return false;
        
       if (!Is_Numeric($_POST['Orders']) || !Is_Numeric($_POST['WorkInProgress']) || !Is_Numeric($_POST['Complete']))
          return false;
        
       if (($_POST['Orders'] < 0) || ($_POST['WorkInProgress'] < 0) || ($_POST['Complete'] < 0))
         return false;
       
        return true;
        
      case 'Add':
          
        if (($_POST['Responsible'] == "") || ($_POST['CostManager'] == "") || ($_POST['Pastel'] == "") || ($_POST['Description'] == "") || ($_POST['Project'] == ""))
          return false;
        break;
      case 'Update':
        if ($_POST['Details'] == "")
          return false;
        break;
      case 'View':
        if ($_POST['Project'] == "")
          return false;
          
        if ($_SESSION['AuthMA'] & 64)
        {
          if (!(CheckDate($_POST['StartMonth'], $_POST['StartDay'], $_POST['StartYear'])) || !(CheckDate($_POST['EndMonth'], $_POST['EndDay'], $_POST['EndYear'])))
            return false;
          
          if (DatabaseDateLater(GetDatabaseDate($_POST['StartDay'], $_POST['StartMonth'], $_POST['StartYear']), GetDatabaseDate($_POST['EndDay'], $_POST['EndMonth'], $_POST['EndYear'])))
            return false;
        }
        break;
      case 'ViewSingle':
        if ($_POST['Project'] == "")
          return false;
        break;
        case 'ViewMapping':
            return true;
            break;
         case 'CostManagers':
            if (($_POST['CostManager'] == "") || ($_POST['CostManager1'] == "") || ($_POST['CostManager2'] == "") || ($_POST['CostManager3'] == "") || ($_POST['CostManager4'] == "") || ($_POST['CostManager5'] == "") || ($_POST['CostManager6'] == "") || ($_POST['CostManager7'] == "") || ($_POST['CostManager8'] == "") || ($_POST['CostManager9'] == "") || ($_POST['CostManager10'] == "") || ($_POST['CostManager12'] == ""))
                 return false; 
            else
                return true;
            break;
      default:
        return false;
        break;
    }
    
    return true;
  }


  function HandleMapping(){
      unset($_SESSION['ViewProjectMapping']);
  }

    function getProjectMappingCode(){
      $sqlString = "SELECT Project_Code as `S4` , (null) as `MM` ,  concat(Project_Pastel_Prefix,' -' ,Project_Description) as Project_NameS4 , '<not mapped>' as `Project_NameMM` FROM S4Admin.Project WHERE Project_Code NOT IN (SELECT Project_CodeS4 FROM S4Admin.ProjectMapping  WHERE Project_CodeS4 IS NOT NULL) 
                    UNION ALL
                    SELECT  (null) as `S4` , Project_Code as `MM` , '<not mapped>' as `Project_NameS4` ,concat(Project_Pastel_Prefix,' -' ,Project_Description) as Project_NameMM  FROM MMAdmin.Project WHERE Project_Code NOT IN (SELECT Project_CodeMM FROM S4Admin.ProjectMapping  WHERE Project_CodeMM IS NOT NULL) 
                    UNION ALL
                    SELECT Project_CodeS4 as `S4` , Project_CodeMM as `MM`, Project_NameS4,Project_NameMM FROM (SELECT * , 
                    IF(Project_CodeS4 IS NULL , NULL, 
                    (SELECT concat(Project_Pastel_Prefix,' -',Project_Description) FROM S4Admin.Project WHERE Project_Code =  Project_CodeS4)) as Project_NameS4,
                    IF(Project_CodeMM IS NULL , NULL, 
                    (SELECT concat(Project_Pastel_Prefix,' -' ,Project_Description) FROM MMAdmin.Project WHERE Project_Code =  Project_CodeMM)) as Project_NameMM  
                    FROM S4Admin.ProjectMapping) as s";

        $resultSet = ExecuteQuery($sqlString);
        $projects = array();
        while ($row = MySQL_Fetch_Array($resultSet)){
            array_push($projects,$row);
        }

        echo json_encode($projects);
    }

  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for a new project.          //
  //////////////////////////////////////////////////////////////////////////////
  function HandleAdd()
  {
    $_SESSION['AddProject'][0] = $_POST['Pastel'];
    $_SESSION['AddProject'][1] = $_POST['Description'];
    $_SESSION['AddProject'][2] = $_POST['Project'];
    $_SESSION['AddProject'][3] = $_POST['Responsible'];
    $_SESSION['AddProject'][4] = $_POST['CostManager'];
    
         
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('AddProject');
        break;
      case 'Submit':
        if (CheckFields())
        {

          // update empty to be person andrew
          if(strcmp($_POST['CostManager'], "") == 0){
              $_POST['CostManager'] = "015"; 
                $_POST['CostManager'] = "015"; 
              $_POST['CostManager'] = "015"; 
          }
            
          $date = Date('Y-m-d H:i:s');
          if (ExecuteQuery('INSERT INTO Project VALUES("", "'.$_POST['Project'].'", "", "", "'.$date.'", "'.$date.'", "", "'.$_POST['Pastel'].'", "'.$_POST['Description'].'", "'.$_POST['Responsible'].'", "'.$_POST['ProjectType'].'", "0", "0", "0", "0", "0","'.$_POST['CostManager'].'","'.$_POST['CostManager'].'","'.$_POST['CostManager'].'","'.$_POST['CostManager'].'","'.$_POST['CostManager'].'","'.$_POST['CostManager'].'","'.$_POST['CostManager'].'","'.$_POST['CostManager'].'","'.$_POST['CostManager'].'","'.$_POST['CostManager'].'","'.$_POST['CostManager'].'","'.$_POST['CostManager'].'","" )'))
    	    {
               $ProjectS4_code = mysql_insert_id();
              
      	    $rowProj = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Project WHERE Project_Code = "'.$ProjectS4_code.'" ORDER BY Project_Code DESC'));
            $rowStaff = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = '.$_POST['Responsible'].''));
            $rowCM = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = '.$_POST['CostManager'].''));

            $content='PROJECT:               '.$rowProj['Project_Pastel_Prefix'].' - '.$rowProj['Project_Description'].Chr(10).
                     'PM:                    '.$rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name'].Chr(10).
                     'COST MANAGER:          '.$rowCM['Staff_First_Name'].' '.$rowCM['Staff_Last_Name'].Chr(10). 
                     'DESCRIPTION:           '.$_POST['Description'];
            
            $email = 'You have been made responsible for a project. The details of the project are as follows:'.Chr(10).$content;
            $emailCM = 'You have been made a cost manager for a project. The details of the project are as follows:'.Chr(10).$content;
            
       $contentHtml= '<BR /><BR />
                    <TABLE border=0>
                      <TR><TD><B>Project:</B></TD><TD>'.$rowProj['Project_Pastel_Prefix'].' - '.$rowProj['Project_Description'].'</TD></TR>
                      <TR><TD><B>PM:</B></TD><TD>'.$rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name'].'</TD></TR>    
                      <TR><TD><B>Cost Manager:</B></TD><TD>'.$rowCM['Staff_First_Name'].' '.$rowCM['Staff_Last_Name'].'</TD></TR>
                      <TR><TD><B>Description:</B></TD><TD>'.$_POST['Description'].'</TD></TR>
                    </TABLE>';
            $html = 'You have been made responsible for a project. The details of the project are as follows:'.$contentHtml;
            $htmlCM = 'You have been made a cost manager for a project. The details of the project are as follows:'.$contentHtml;
            
            SendMailHTML($rowStaff['Staff_email'], 'Project Added - '.$rowProj['Project_Pastel_Prefix'].' - '.$rowProj['Project_Description'], $email, $html);
            SendMailHTML($rowCM['Staff_email'], 'Project Added - '.$rowProj['Project_Pastel_Prefix'].' - '.$rowProj['Project_Description'], $emailCM, $htmlCM);
            
            
              $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['MAUID'].''));
              
              $email = 'Project details have been added. The details are as follows:'.Chr(10).
                       'PROJECT:               '.$_POST['Project'].Chr(10).
                       'PASTEL PREFIX:         '.$_POST['Pastel'].Chr(10).
                       'ADDED BY:              '.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].Chr(10).Chr(10).
                       'Please review the new details to ensure that everything is in order.';
              
              $html = 'Project details have been added. The details are as follows:
                      <BR /><BR />
                      <TABLE border=0>
                        <TR><TD><B>Project:</B></TD><TD>'.$_POST['Project'].'</TD></TR>
                        <TR><TD><B>Pastel Prefix:</B></TD><TD>'.$_POST['Pastel'].'</TD></TR>
                        <TR><TD><B>Added By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                      </TABLE>
                      <BR />
                      Please review the new details to ensure that everything is in order.
                      <BR /><BR />';
             
                CCSomeone($row['Staff_email'], 'roline@moduasm.co.za', 'Project Added - Modular Assembly -'.$rowProj['Project_Pastel_Prefix'].' - '.$rowProj['Project_Description'], $email, $html);
            
            $_SESSION['ProjectSuccess'] = 'geh!';
            Session_Unregister('AddProject');
          } else
    	      $_SESSION['ProjectFail'] = 'geh!';
        } else
          $_SESSION['ProjectIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of modified information for a project.     //
  //////////////////////////////////////////////////////////////////////////////
  function HandleEdit()
  {
    $_SESSION['EditProject'][1] = $_POST['Pastel'];
    $_SESSION['EditProject'][2] = $_POST['Description'];
    $_SESSION['EditProject'][3] = $_POST['Project'];
    $_SESSION['EditProject'][4] = $_POST['Responsible'];
    $_SESSION['EditProject'][5] = $_POST['CostManager'];
    $_SESSION['EditProject'][6] = $_POST['Orders'];
    $_SESSION['EditProject'][7] = $_POST['WorkInProgress'];
    $_SESSION['EditProject'][8] = $_POST['Complete'];
    $_SESSION['EditProject'][9] = $_POST['Closed'];
    
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('EditProject');
        break;
      case 'Submit':
        if (CheckFields())
        {
          $emails = array();

          // update empty to be person responsible
          if(strcmp($_POST['CostManager'], "") == 0){
              $_POST['CostManager'] = "015"; 
                $_POST['CostManager'] = "015"; 
              $_POST['CostManager'] = "015"; 
          }
            
          $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Project WHERE Project_Code = "'.$_SESSION['EditProject'][0].'"'));
          $rowCM = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = '.$row['Project_CostManager'].''));
          $previousNumber = $row['Project_Pastel_Prefix'].' '.$row['Project_Description'];
          $previousResponsible = $row['Project_Responsible'];
     
          array_push($emails, $rowCM['Staff_email']);
            
          if (ExecuteQuery('UPDATE Project SET Project_Description = "'.$_POST['Project'].'", Project_Pastel_Prefix = "'.$_POST['Pastel'].'", Project_Long_Description = "'.$_POST['Description'].'", Project_Responsible = "'.$_POST['Responsible'].'", Project_Closed = "'.$_POST['Closed'].'", Project_OrdersPlaced = "'.$_POST['Orders'].'", Project_WorkInProgress = "'.$_POST['WorkInProgress'].'", Project_Complete = "'.$_POST['Complete'].'", Project_DateTime_Updated = "'.Date('Y-m-d').'", Project_CostManager = "'.$_POST['CostManager'].'" , Project_CostManager1 = "'.$_POST['CostManager'].'" , Project_CostManager2 = "'.$_POST['CostManager'].'", Project_CostManager3 = "'.$_POST['CostManager'].'" , Project_CostManager4 = "'.$_POST['CostManager'].'" , Project_CostManager5 = "'.$_POST['CostManager'].'" , Project_CostManager6 = "'.$_POST['CostManager'].'" , Project_CostManager7 = "'.$_POST['CostManager'].'" , Project_CostManager8 = "'.$_POST['CostManager'].'" , Project_CostManager9 = "'.$_POST['CostManager'].'" , Project_CostManager10 = "'.$_POST['CostManager'].'" , Project_CostManager12 = "'.$_POST['CostManager'].'" WHERE Project_Code = "'.$_SESSION['EditProject'][0].'"'))
    	    {
        	  $rowProj = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Project WHERE Project_Code = "'.$_SESSION['EditProject'][0].'"'));
            $rowStaff = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = "'.$_POST['Responsible'].'"'));
            $rowCM = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = "'.$_POST['CostManager'].'"'));
            
            if ($previousResponsible != $_POST['Responsible'])
            {
              $rowTemp = MySQL_Fetch_Array(ExecuteQuery('SELECT Staff_First_Name, Staff_Last_Name, Staff_email FROM Staff WHERE Staff_Code = "'.$previousResponsible.'"'));
              $html = 'You are no longer responsible for the following project: <B>'.$previousNumber.'</B>. This project has been handed over to <B>'.$rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name'].'</B>.';
               if(strcmp($rowTemp['Staff_email'], "") != 0){
                SendMailHTML($rowTemp['Staff_email'], 'Project Updated - '.$rowProj['Project_Pastel_Prefix'].' - '.$rowProj['Project_Description'], "", $html);
               }
            }   
            
            $html = 'The following project has been changed: '.$previousNumber.'. The new details of the project are as follows:
                    <BR /><BR />
                    <TABLE border=0>
                      <TR><TD><B>Project:</B></TD><TD>'.$rowProj['Project_Pastel_Prefix'].' - '.$rowProj['Project_Description'].'</TD></TR>
                      <TR><TD><B>PM:</B></TD><TD>'.$rowStaff['Staff_First_Name'].' '.$rowStaff['Staff_Last_Name'].'</TD></TR>
                      <TR><TD><B>Cost Manager:</B></TD><TD>'.$rowCM['Staff_First_Name'].' '.$rowCM['Staff_Last_Name'].'</TD></TR>
                      <TR><TD><B>Description:</B></TD><TD>'.$_POST['Description'].'</TD></TR>
                    </TABLE>';
                
            array_push($emails, $rowStaff['Staff_email']);
            array_push($emails, $rowCM['Staff_email']);
              
            $emails = array_unique($emails);
            foreach ($emails as $email){
                if(strcmp($email, "") != 0){
                      SendMailHTML($email, 'Project Updated - '.$rowProj['Project_Pastel_Prefix'].' - '.$rowProj['Project_Description'], "", $html);
                }
            }
            
            $row = MySQL_Fetch_Array(ExecuteQuery('SELECT * FROM Staff WHERE Staff_Code = '.$_SESSION['MAUID'].''));
            
            $html = 'Project details have been updated. The details are as follows:
                    <BR /><BR />
                    <TABLE border=0>
                      <TR><TD><B>Project:</B></TD><TD>'.$_POST['Project'].'</TD></TR>
                      <TR><TD><B>Pastel Prefix:</B></TD><TD>'.$_POST['Pastel'].'</TD></TR>
                      <TR><TD><B>Updated By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                    </TABLE>
                    <BR />
                    Please review the updated details to ensure that everything is in order.
                    <BR /><BR />';
              
            CCSomeone($row['Staff_email'], 'roline@moduasm.co.za', 'Project Updated - '.$rowProj['Project_Pastel_Prefix'].' - '.$rowProj['Project_Description'], "", $html);
              
            if ($_POST['Closed'] == '1')
            {
              $html = 'Project has been closed. The details are as follows:
                      <BR /><BR />
                      <TABLE border=0>
                        <TR><TD><B>Project:</B></TD><TD>'.$_POST['Project'].'</TD></TR>
                        <TR><TD><B>Pastel Prefix:</B></TD><TD>'.$_POST['Pastel'].'</TD></TR>
                        <TR><TD><B>Closed By:</B></TD><TD>'.$row['Staff_First_Name'].' '.$row['Staff_Last_Name'].'</TD></TR>
                      </TABLE>';
              
              CCSomeone($row['Staff_email'], 'roline@moduasm.co.za', 'Project Closed - '.$rowProj['Project_Pastel_Prefix'].' - '.$rowProj['Project_Description'], "", $html);
            }
            
            $_SESSION['ProjectSuccess'] = 'geh!';
            Session_Unregister('EditProject');
          } else
    	      $_SESSION['ProjectFail'] = 'geh!';
        } else
          $_SESSION['ProjectIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's maintenance selection.                                //
  //////////////////////////////////////////////////////////////////////////////
  function HandleMaintain()
  {
    switch ($_POST['Submit'])
    {
      case 'Add':
        $_SESSION['AddProject'] = array();
        $_SESSION['AddProject'][3] = $_SESSION['MAUID'];
            
        break;
      case 'Edit':
        if ($_POST['ProjectEdit'] == "")
          $_SESSION['ProjectIncomplete'] = 'geh!';
        else
        {
            
            $sql = 'SELECT * FROM Project WHERE Project_Code ='.$_POST['ProjectEdit'];
 
          $row = MySQL_Fetch_Array(ExecuteQuery($sql));
         
          $_SESSION['EditProject'] = array($_POST['ProjectEdit']);
          $_SESSION['EditProject'][1] = $row['Project_Pastel_Prefix'];
          $_SESSION['EditProject'][2] = $row['Project_Long_Description'];
          $_SESSION['EditProject'][3] = $row['Project_Description'];
          $_SESSION['EditProject'][4] = $row['Project_Responsible'];
          $_SESSION['EditProject'][5] = $row['Project_CostManager'];
          $_SESSION['EditProject'][6] = $row['Project_OrdersPlaced'];
          $_SESSION['EditProject'][7] = $row['Project_WorkInProgress'];
          $_SESSION['EditProject'][8] = $row['Project_Complete'];
          $_SESSION['EditProject'][9] = $row['Project_Closed'];
          
        }
        break;
      case 'Update':
        if ($_POST['ProjectUpdate'] == "")
          $_SESSION['ProjectIncomplete'] = 'geh!';
        else
        {
          $_SESSION['UpdateProject'] = array();
          $_SESSION['UpdateProject'][100] = $_POST['ProjectUpdate'];
        }
        break;
      case 'Assign':
               
           $sql = 'SELECT  * FROM ReportsCategory';
           $resultset = ExecuteQuery($sql);

            while($row = MySQL_Fetch_Array($resultset)){
                $id = (int)$row['ReportsCategory_ID'];
                $_SESSION['ViewCostMangers'][$id] = $row['Person_Responsible'];
            }   

        break;   
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of information for project history.        //
  //////////////////////////////////////////////////////////////////////////////
  function HandleUpdate()
  {
    $_SESSION['UpdateProject'][0] = $_POST['Details'];
    
    switch ($_POST['Submit'])
    {
      case 'Cancel':
        Session_Unregister('UpdateProject');
        break;
      case 'Submit':
        if (CheckFields())
        {
          $date = Date('Y-m-d H:i:s');
          if (ExecuteQuery('INSERT INTO ProjectLog VALUES("", "'.$_SESSION['UpdateProject'][100].'", "'.$_POST['Details'].'", "'.$_SESSION['MAUID'].'", "'.$date.'")'))
    	    {
            $_SESSION['ProjectSuccess'] = 'geh!';
            Session_Unregister('UpdateProject');
          } else
    	      $_SESSION['ProjectFail'] = 'geh!';
        } else
          $_SESSION['ProjectIncomplete'] = 'geh!';
        break;
      default:
        break;
    }
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // Handles the user's submission of a project selection.                    //
  //////////////////////////////////////////////////////////////////////////////
  function HandleView()
  {
    if (CheckFields())
    {
      switch ($_POST['Type'])
      {
        case 'View':
          $_SESSION['ViewProject'] = array($_POST['Project']);
          $_SESSION['ViewProject'][1] = $_POST['ProjectStatus'];
          $_SESSION['ViewProject'][2] = $_POST['Name'];
          $_SESSION['ViewProject'][3] = $_POST['ProjectType'];
          $_SESSION['ViewProject'][4] = $_POST['StartYear'].$_POST['StartMonth'].$_POST['StartDay'];
          $_SESSION['ViewProject'][5] = $_POST['EndYear'].$_POST['EndMonth'].$_POST['EndDay'];
          break;
        case 'ViewSingle':
          $_SESSION['ViewProjectSingle'] = array();
          $_SESSION['ViewProjectSingle'][100] = $_POST['Project'];
          break;
          case 'ViewMapping':
              $_SESSION['ViewProjectMapping'] = array();
              break;
        default:
          break;
      }
    } else
      $_SESSION['ProjectIncomplete'] = 'geh!';
  }
  
  
  function HandleCostManager(){

       switch ($_POST['Submit'])
      {
        case 'Cancel':
         unset($_SESSION['ViewCostMangers']);
          break;
        case 'Submit':
        
   
          $_SESSION['ViewCostMangers'][0] =  $_POST['CostManager'];
          $_SESSION['ViewCostMangers'][1] =  $_POST['CostManager1'];
          $_SESSION['ViewCostMangers'][2] =  $_POST['CostManager2'];
          $_SESSION['ViewCostMangers'][3] =  $_POST['CostManager3'];
          $_SESSION['ViewCostMangers'][4] =  $_POST['CostManager4'];
          $_SESSION['ViewCostMangers'][5] =  $_POST['CostManager5'];
          $_SESSION['ViewCostMangers'][6] =  $_POST['CostManager6'];
          $_SESSION['ViewCostMangers'][7] =  $_POST['CostManager7'];
          $_SESSION['ViewCostMangers'][8] =  $_POST['CostManager8'];
          $_SESSION['ViewCostMangers'][9] =  $_POST['CostManager9'];
          $_SESSION['ViewCostMangers'][10] =  $_POST['CostManager10'];
          $_SESSION['ViewCostMangers'][12] =  $_POST['CostManager12'];
            
           if(CheckFields()){ 
               
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager'].'" WHERE ReportsCategory_ID = 0');  
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager1'].'" WHERE ReportsCategory_ID = 1');  
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager2'].'" WHERE ReportsCategory_ID = 2');  
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager3'].'" WHERE ReportsCategory_ID = 3');
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager4'].'" WHERE ReportsCategory_ID = 4');
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager5'].'" WHERE ReportsCategory_ID = 5');
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager6'].'" WHERE ReportsCategory_ID = 6');
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager7'].'" WHERE ReportsCategory_ID = 7');
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager8'].'" WHERE ReportsCategory_ID = 8');
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager9'].'" WHERE ReportsCategory_ID = 9');
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager10'].'" WHERE ReportsCategory_ID = 10');
//                ExecuteQuery('UPDATE ReportsCategory SET Person_Responsible = "'.$_POST['CostManager12'].'" WHERE ReportsCategory_ID = 12');

               
               $sql = 'UPDATE ReportsCategory
                        SET Person_Responsible = (case when ReportsCategory_ID = 0 then "'.$_POST['CostManager'].'"
                                             when ReportsCategory_ID = 1 then "'.$_POST['CostManager1'].'"
                                             when ReportsCategory_ID = 2 then "'.$_POST['CostManager2'].'"
                                             when ReportsCategory_ID = 3 then "'.$_POST['CostManager3'].'"
                                             when ReportsCategory_ID = 4 then "'.$_POST['CostManager4'].'" 
                                             when ReportsCategory_ID = 5 then "'.$_POST['CostManager5'].'"
                                             when ReportsCategory_ID = 6 then "'.$_POST['CostManager6'].'"
                                             when ReportsCategory_ID = 7 then "'.$_POST['CostManager7'].'"
                                             when ReportsCategory_ID = 8 then "'.$_POST['CostManager8'].'"    
                                             when ReportsCategory_ID = 9 then "'.$_POST['CostManager9'].'"
                                             when ReportsCategory_ID = 10 then "'.$_POST['CostManager10'].'"
                                             when ReportsCategory_ID = 12 then "'.$_POST['CostManager12'].'"
  
                                        end)';
               if(ExecuteQuery($sql)){
                   unset($_SESSION['ViewCostMangers']);  
                   $_SESSION['ProjectSuccess'] = 'geh!'; 
               }
               else{
                     $_SESSION['ProjectFail'] = 'geh!'; 
               } 
           }
           else{
                $_SESSION['ProjectIncomplete'] = 'geh!';
           }
          
          break;
        default:
          break;
      }
  }

?>