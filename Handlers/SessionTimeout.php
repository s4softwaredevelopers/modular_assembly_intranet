<?php
/**
 * Created by PhpStorm.
 * User: Modular Assembly
 * Date: 2018/08/07
 * Time: 12:29
 */
include '../Scripts/Include.php';
SetSettings();
$_SESSION['NotLoggedIn'] = true;
if (isset($_SESSION['AuthMA']))
{
    Session_Unregister('AuthMA');
    Session_Unregister('MAUID');
}
?>