<?php 
  // DETAILS ///////////////////////////////////////////////////////////////////
  //                                                                          //
  //                    Last Edited By: Gareth Ambrose                        //
  //                        Date: 05 December 2007                            //
  //                                                                          //
  ////////////////////////////////////////////////////////////////////////////// 
  // This page manages user authentication and logging out.                   //
  //////////////////////////////////////////////////////////////////////////////  
   
  include '../Scripts/Include.php';
  SetSettings();
                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
  if (isset($_SESSION['AuthMA']))
  {
    Session_Unregister('AuthMA');
    Session_Unregister('MAUID');
  } 
  
  Header('Location: ../Login.php');
?>
